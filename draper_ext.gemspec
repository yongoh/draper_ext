$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "draper_ext/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "draper_ext"
  s.version     = DraperExt::VERSION
  s.authors     = ["yongoh"]
  s.email       = ["a.yongoh@gmail.com"]
  s.homepage    = ""
  s.summary     = ""
  s.description = ""
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "draper_ext-model"
  s.add_dependency "draper_ext-form"
  s.add_dependency "draper_ext-attribute"
  s.add_dependency "draper_ext-field"
  s.add_dependency "draper_ext-association"
  s.add_dependency "draper_ext-association_form"
  s.add_dependency "draper_ext-ransack"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "database_cleaner"
  s.add_development_dependency "factory_bot_rails"
  s.add_development_dependency "rspec-html-matchers"
  s.add_development_dependency "capybara", "3.15"
  s.add_development_dependency "sass"
  s.add_development_dependency "jbuilder"
end
