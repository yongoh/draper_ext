command_arguments = %w(
  name:string
  sex:boolean
  height:decimal
  parent:belongs_to
  children:has_many
  wife:belongs_to
  husband:has_one
  friends:has_and_belongs_to_many
)

# `--skip-orm`によりモデル・ファクトリージェネレータのテストはできない
command_options = %w(
  --no-stylesheets
  --skip-assets
  --skip-orm
  --skip-helper
  --test-framework=rspec
  --controller-specs
)

namespace :generators do
  namespace :scaffold do
    desc "Generate scaffold animal"
    task :generate do
      sh "rails generate scaffold animal #{command_arguments.join(' ')} #{command_options.join(' ')}"
    end

    desc "Destroy scaffold animal"
    task :destroy do
      sh "bundle exec rails destroy scaffold animal #{command_options.join(' ')}"
    end

    desc "Test scaffold animal"
    task :test do
      sh "rspec spec/requests spec/routing spec/decorators spec/views spec/controllers"
    end

    desc "Generate, test and destroy"
    task :default => [:generate, :test, :destroy]
  end
end
