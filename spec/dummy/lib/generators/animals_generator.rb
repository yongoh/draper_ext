class AnimalsGenerator < Rails::Generators::Base

  def gsub_controller_file
    filepath = "app/controllers/animals_controller.rb"

    insert_into_file filepath, ".includes(:parent, :children, :wife, :husband, :friends)", after: /@ransack\.result\b/

    gsub_file filepath, /^\s*def animal_params\n.+?end$/m do |matched|
      matched.match(/\n(\s*?)def /)
      <<~EOF
      def animal_params
        params.fetch(:animal, {}).permit(
          :name, :sex, :height,
          :parent_id,
          :spouse_id,
          husband_attributes: [:id, :_destroy],
          child_ids: [],
          friend_ids: [],
        )
      end
      EOF
      .gsub(/^/, $1).gsub(/\A/, "\n").gsub(/\n\z/, "")
    end
  end

  def gsub_model_decorator_file
    filepath = "app/decorators/animals/animal_model_decorator.rb"

    gsub_file filepath, /^\s*associations\b.+$/ do |matched|
      matched.match(/\A(\s*)/)
      <<~EOF
      association :parent, reverse: :children
      association :children
      association :wife, reverse: :husband
      association :husband, reverse: :wife
      association :friends, reverse: :friends
      EOF
      .gsub(/^/, $1).gsub(/\A/, "\n")
    end

    insert_into_file filepath, "    block ||= :name.to_proc\n", after: /\bdef banner\b.*?\n/
  end

  def insert_into_form_view_file
    insert_into_file "app/views/animals/_form.html.erb", before: /\z/ do
      <<~EOF
      <%= reference_finder "animals" do |x| %>
        <%= x.finder("list", route_key: Animal.model_name.route_key) do %>
          <%- Animal.all.each do |record| -%>
            <li>
              <%= choice_button(record) %>
              <%= record.decorate.banner %>
            </li>
          <%- end -%>
        <%- end -%>
      <% end %>
      EOF
      .gsub(/\A/, "\n")
    end
  end
end
