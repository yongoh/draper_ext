class PersonModelDecorator < Draper::Decorator
  include DraperExt::Model
  attributes :name, :height

  def banner(*args)
    h.record_banner(object, *args, &:name)
  end
end
