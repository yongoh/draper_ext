class PersonSearchFormDecorator < Draper::Decorator
  include DraperExt::Ransack::SearchForm
end
