class Person < ApplicationRecord
  validates :name, presence: true
  validates :height, numericality: true

  belongs_to :parent, class_name: "Person", required: false
  belongs_to :wife, class_name: "Person", foreign_key: :spouse_id, required: false
  has_one :husband, class_name: "Person", foreign_key: :spouse_id
  has_one :grandparent, through: :parent, source: :parent
  has_many :children, class_name: "Person", foreign_key: :parent_id
  has_many :grandchildren, through: :children, source: :children
  has_and_belongs_to_many :friends, class_name: "Person", join_table: :friends, foreign_key: :from_id, association_foreign_key: :to_id

  accepts_nested_attributes_for :parent, :children, allow_destroy: true
  accepts_nested_attributes_for :friends
end
