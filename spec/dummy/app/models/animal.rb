class Animal < ApplicationRecord
  validates :name, presence: true
  validates :sex, inclusion: {in: [true, false]}
  validates :height, numericality: true

  belongs_to :parent, class_name: "Animal", required: false
  has_many :children, class_name: "Animal", foreign_key: :parent_id
  has_one :husband, class_name: "Animal", foreign_key: :spouse_id
  belongs_to :wife, class_name: "Animal", required: false, foreign_key: :spouse_id
  has_and_belongs_to_many :friends, class_name: "Animal", join_table: :friends, foreign_key: :from_id, association_foreign_key: :to_id

  accepts_nested_attributes_for :parent, :children, :husband, :friends, allow_destroy: true
end
