# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_09_12_211811) do

  create_table "animals", force: :cascade do |t|
    t.string "name"
    t.decimal "height"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "parent_id"
    t.integer "spouse_id"
    t.boolean "sex", default: true, null: false
    t.index ["parent_id"], name: "index_animals_on_parent_id"
    t.index ["spouse_id"], name: "index_animals_on_spouse_id"
  end

  create_table "friends", force: :cascade do |t|
    t.integer "from_id"
    t.integer "to_id"
    t.index ["from_id"], name: "index_friends_on_from_id"
    t.index ["to_id"], name: "index_friends_on_to_id"
  end

  create_table "people", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "parent_id"
    t.integer "spouse_id"
    t.decimal "height"
    t.index ["parent_id"], name: "index_people_on_parent_id"
    t.index ["spouse_id"], name: "index_people_on_spouse_id"
  end

end
