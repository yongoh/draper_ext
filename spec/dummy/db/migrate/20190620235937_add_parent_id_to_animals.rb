class AddParentIdToAnimals < ActiveRecord::Migration[5.0]
  def change
    change_table :animals do |t|
      t.references :parent
    end
  end
end
