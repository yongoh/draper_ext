class AddParentIdToPerson < ActiveRecord::Migration[5.0]
  def change
    change_table :people do |t|
      t.belongs_to :parent, index: true
    end
  end
end
