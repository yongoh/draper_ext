class AddSpouseIdToPeople < ActiveRecord::Migration[5.0]
  def change
    change_table :people do |t|
      t.belongs_to :spouse, index: true
    end
  end
end
