class AddHeightToPeople < ActiveRecord::Migration[5.0]
  def change
    change_table :people do |t|
      t.decimal :height
    end
  end
end
