class CreateFriends < ActiveRecord::Migration[5.0]
  def change
    create_table :friends do |t|
      t.belongs_to :from
      t.belongs_to :to
    end
  end
end
