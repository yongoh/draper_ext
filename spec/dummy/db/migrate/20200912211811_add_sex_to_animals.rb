class AddSexToAnimals < ActiveRecord::Migration[6.0]
  def change
    change_table :animals do |t|
      t.boolean :sex, null: false, default: true
    end
  end
end
