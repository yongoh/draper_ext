require 'rails_helper'

describe Person do
  describe "#parent (belongs_to)" do
    let(:person){ create(:person, parent_id: parent.id) }
    let(:parent){ create(:person) }
    it{ expect(person.reload.parent).to eq(parent) }
  end

  describe "#wife (belongs_to)" do
    let(:person){ create(:person, spouse_id: wife.id) }
    let(:wife){ create(:person) }
    it{ expect(person.reload.wife).to eq(wife) }
  end

  describe "#husband (has_one)" do
    let(:person){ create(:person) }
    let(:husband){ create(:person, spouse_id: person.id) }
    before{ husband }
    it{ expect(person.reload.husband).to eq(husband) }
  end

  describe "#grandparent (has_one through)" do
    let(:person){ create(:person, parent_id: parent.id) }
    let(:parent){ create(:person, parent_id: grandparent.id) }
    let(:grandparent){ create(:person) }
    it{ expect(person.reload.grandparent).to eq(grandparent) }
  end

  describe "#children (has_many)" do
    let(:person){ create(:person) }
    let(:children){ create_list(:person, 3, parent_id: person.id) }
    before{ children }
    it{ expect(person.reload.children.to_a).to eq(children) }
  end

  describe "#grandchildren (has_many through)" do
    let(:person){ create(:person) }
    let(:children){ create_list(:person, 3, parent_id: person.id) }
    let(:grandchildren){ children.map{|child| create_list(:person, 3, parent_id: child.id) }.flatten }
    before{ grandchildren }
    it{ expect(person.reload.grandchildren.to_a).to eq(grandchildren) }
  end

  describe "#friends (has_and_belongs_to_many)" do
    let(:person){ create(:person, friends: friends) }
    let(:friends){ create_list(:person, 5) }
    it{ expect(person.reload.friends.to_a).to eq(friends) }
  end
end
