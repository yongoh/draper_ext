FactoryBot.define do
  factory :animal do
    sequence(:name){|n| "Name-#{n}"}
    sequence(:sex){ [true, false].sample }
    sequence(:height){ rand(150.0..190.0).round(1) }

    %w(parent wife husband).each do |association_name|
      trait "with_#{association_name}" do
        association association_name, factory: :animal
      end
    end

    trait :with_children do
      children{ create_list(:animal, 3) }
    end

    trait :with_friends do
      friends{ create_list(:animal, 3) }
    end

    trait :invalid do
      name{ "" }
      sex{ nil }
      height{ "百六十八" }
    end
  end
end
