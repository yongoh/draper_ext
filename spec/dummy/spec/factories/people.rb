FactoryBot.define do
  factory :person do
    sequence(:name){|n| "Name-#{n}"}
    sequence(:height){ rand(150.0..190.0).round(1) }

    %w(parent wife husband).each do |association_name|
      trait "with_#{association_name}" do
        association association_name, factory: :person
      end
    end

    trait :with_children do
      children{ create_list(:person, 3) }
    end

    trait :with_grandparent do
      parent{ create(:person, :with_parent) }
    end

    trait :with_friends do
      friends{ create_list(:person, 3) }
    end

    trait :invalid do
      name{ "" }
      height{ "百六十八" }
    end
  end
end
