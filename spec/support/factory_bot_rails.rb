require 'factory_bot_rails'

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
end

if FactoryBot.definition_file_paths.none?{|path| Dir.exists?(path) }
  FactoryBot.definition_file_paths << Rails.root.join('spec', 'factories').to_s
  FactoryBot.find_definitions
end
