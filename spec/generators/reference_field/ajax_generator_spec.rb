require 'rails_helper'

feature "rails generate reference_field:ajax" do
  before do
    Dir.chdir(Rails.root)
    remove_files
    `rake -s generators:scaffold:generate`
  end

  after do
    remove_files
  end

  def remove_files
    FileUtils.rm_r(generated_paths) rescue Errno::ENOENT
    `rake -s generators:scaffold:destroy`
  end

  let(:app_view_path){ "app/views/animals/ajax/_index.html.erb" }
  let(:spec_view_path){ "spec/views/animals/ajax/_index.html.erb_spec.rb" }

  let(:generated_paths){ [app_view_path, spec_view_path] }

  feature "`generate`コマンドを実行" do
    before do
      stdout = `rails g reference_field:ajax animal --test-framework=rspec`
      puts stdout unless $?.success?
    end

    scenario "各ファイルを作成" do
      generated_paths.each do |path|
        expect(File).to be_exist(path)
      end
    end

    feature "生成したテストを実行" do
      scenario "テスト成功" do
        stdout = `rspec #{spec_view_path} spec/requests/animals_spec.rb`
        expect(stdout).not_to match(/\b0 examples/)
        expect(stdout).to match(/\b0 failures/)
      end
    end

    feature "`destroy`コマンドを実行" do
      before do
        stdout = `rails d reference_field:ajax animal --test-framework=rspec`
        puts stdout unless $?.success?
      end

      scenario "生成した各ファイルを削除" do
        generated_paths.each do |path|
          expect(File).not_to be_exist(path)
        end
      end
    end
  end
end
