require 'rails_helper'

feature "rails generate scaffold" do
  before do
    Dir.chdir(Rails.root)
    remove_files
  end

  after do
    remove_files
  end

  def remove_files
    FileUtils.rm_r(generated_roots) rescue Errno::ENOENT
  end

  let(:controller_app_file){ "app/controllers/animals_controller.rb" }
  let(:controller_spec_file){ "spec/controllers/animals_controller_spec.rb" }
  let(:decorator_app_root){ "app/decorators/animals" }
  let(:decorator_spec_root){ "spec/decorators/animals" }
  let(:view_app_root){ "app/views/animals" }
  let(:view_spec_root){ "spec/views/animals" }
  let(:integration_spec_file){ "spec/requests/animals_spec.rb" }
  let(:routing_spec_file){ "spec/routing/animals_routing_spec.rb" }

  let(:generated_roots){
    [
      controller_app_file,
      controller_spec_file,
      decorator_app_root,
      decorator_spec_root,
      view_app_root,
      view_spec_root,
      integration_spec_file,
      routing_spec_file,
    ]
  }

  let(:generated_files){
    %w(
      app/controllers/animals_controller.rb
      app/decorators/animals/animal_model_decorator.rb
      app/decorators/animals/animal_form_decorator.rb
      app/decorators/animals/animal_table_decorator.rb
      app/decorators/animals/animal_article_decorator.rb
      app/decorators/animals/animal_form_decorator.rb
      app/decorators/animals/animal_search_form_decorator.rb
      app/views/animals/index.html.erb
      app/views/animals/show.html.erb
      app/views/animals/_form.html.erb
      app/views/animals/search.html.erb
      app/views/animals/_animal.json.jbuilder
      spec/decorators/animals/animal_model_decorator_spec.rb
      spec/decorators/animals/animal_table_decorator_spec.rb
      spec/decorators/animals/animal_article_decorator_spec.rb
      spec/decorators/animals/animal_form_decorator_spec.rb
      spec/decorators/animals/animal_search_form_decorator_spec.rb
      spec/views/animals/index.html.erb_spec.rb
      spec/views/animals/show.html.erb_spec.rb
      spec/views/animals/edit.html.erb_spec.rb
      spec/views/animals/new.html.erb_spec.rb
      spec/views/animals/search.html.erb_spec.rb
      spec/views/animals/index.json.jbuilder_spec.rb
      spec/views/animals/show.json.jbuilder_spec.rb
    ) + [
      controller_app_file,
      controller_spec_file,
      integration_spec_file,
      routing_spec_file,
    ]
  }

  feature "`generate`コマンドを実行" do
    before do
      stdout = `rake -s generators:scaffold:generate`
      puts stdout unless $?.success?
    end

    scenario "各ファイルを作成" do
      generated_files.each do |path|
        expect(File).to be_exist(path)
      end
    end

    feature "生成したテストを実行" do
      scenario "テスト成功" do
        stdout = `rake -s generators:scaffold:test`
        expect(stdout).not_to match(/\b0 examples/)
        expect(stdout).to match(/\b0 failures/)
      end
    end

    feature "`destroy`コマンドを実行" do
      before do
        stdout = `rake -s generators:scaffold:destroy`
        puts stdout unless $?.success?
      end

      scenario "生成した各ファイルを削除" do
        generated_roots.each do |path|
          expect(path.match(/\.rb$/) ? File : Dir).not_to be_exist(path)
        end
      end
    end
  end
end
