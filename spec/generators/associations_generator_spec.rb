require 'rails_helper'

feature "rails generate associations" do
  before do
    Dir.chdir(Rails.root)
    remove_files
    `rails generate scaffold animal #{scaffold_command_options.join(' ')}`
  end

  let(:scaffold_command_options){
    %w(
      --no-stylesheets
      --skip-assets
      --skip-orm
      --skip-helper
      --test-framework=rspec
    )
  }

  after do
    `rails destroy scaffold animal #{scaffold_command_options.join(' ')}`
  end

  def remove_files
    FileUtils.rm_r(generated_roots) rescue Errno::ENOENT
  end

  def command_generate
    stdout = `rails generate associations animal #{command_arguments.join(' ')} #{command_options.join(' ')}`
    puts stdout unless $?.success?
    stdout
  end

  def command_destroy
    stdout = `rails destroy associations animal #{command_arguments.join(' ')} #{command_options.join(' ')}`
    puts stdout unless $?.success?
    stdout
  end

  let(:command_arguments){
    %w(
      parent:belongs_to
      children:has_many
    )
  }

  # `--skip-orm`によりモデル・ファクトリージェネレータのテストはできない
  let(:command_options){
    %w(
      --skip-orm
      --test-framework=rspec
    )
  }

  let(:generated_roots){
    %w(
      app/controllers/animals_controller.rb
      app/decorators/animals
      app/views/animals
      spec/decorators/animals
      spec/views/animals
    )
  }

  let(:inserted_files){
    %w(
      app/controllers/animals_controller.rb
      app/decorators/animals/animal_model_decorator.rb
      app/decorators/animals/animal_form_decorator.rb
      app/decorators/animals/animal_table_decorator.rb
      app/decorators/animals/animal_article_decorator.rb
      app/decorators/animals/animal_form_decorator.rb
      app/decorators/animals/animal_search_form_decorator.rb
      app/views/animals/index.html.erb
      app/views/animals/show.html.erb
      app/views/animals/_form.html.erb
      app/views/animals/search.html.erb
      app/views/animals/_animal.json.jbuilder
      spec/decorators/animals/animal_model_decorator_spec.rb
      spec/decorators/animals/animal_table_decorator_spec.rb
      spec/decorators/animals/animal_article_decorator_spec.rb
      spec/decorators/animals/animal_form_decorator_spec.rb
      spec/decorators/animals/animal_search_form_decorator_spec.rb
      spec/views/animals/index.html.erb_spec.rb
      spec/views/animals/show.html.erb_spec.rb
      spec/views/animals/edit.html.erb_spec.rb
      spec/views/animals/new.html.erb_spec.rb
      spec/views/animals/search.html.erb_spec.rb
      spec/views/animals/index.json.jbuilder_spec.rb
      spec/views/animals/show.json.jbuilder_spec.rb
    )
  }

  let(:spec_paths){
    %w(
      spec/requests/animals_spec.rb
      spec/decorators/animals
      spec/views/animals
    )
  }

  feature "`generate`コマンドを実行" do
    subject do
      command_generate
    end

    scenario "各ファイルに追記する" do
      inserted_files.each do |path|
        is_expected.to match(%r{\binsert\s+#{path}})
      end
      is_expected.not_to include("spec/controllers/animals_controller_spec.rb")
    end

    feature "テストを実行" do
      subject do
        `rspec #{spec_paths.join(' ')}`
      end

      before do
        command_generate
        # HACK: コントローラテスト用の属性を加える処理はもっと効率の良いやり方があるはずだ
        `rails generate rails:scaffold_controller:attributes animal name:string sex:boolean height:decimal`
      end

      scenario "テスト成功" do
        is_expected.not_to match(/\b0 examples/)
        is_expected.to match(/\b0 failures/)
      end
    end

    feature "`destroy`コマンドを実行" do
      subject do
        command_destroy
      end

      before do
        command_generate
      end

      scenario "各ファイルから削除する" do
        inserted_files.each do |path|
          is_expected.to match(%r{\bsubtract\s+#{path}})
        end
        is_expected.not_to include("spec/controllers/animals_controller_spec.rb")
      end
    end
  end
end
