require "draper_ext/decoration"

require "draper_ext/form/railtie"
require "draper_ext/form/form_helper"

module DraperExt
  module Form
    extend ActiveSupport::Concern
    include Markup

    module ClassMethods
    end

    # @!attribute [r] form_builder
    #   @return [ActionView::Helpers::FormBuilder] form_builder フォームビルダー
    attr_reader :form_builder
    alias :f :form_builder
    delegate :model_name, to: :object

    def initialize(form_builder, options = {})
      @form_builder = form_builder
      super(form_builder.object, options)
    end

    # @return [ActiveSupport::SafeBuffer] エラー説明欄
    def error_explanation_about
      m.div(class: "error_explanation") do |m|
        m.h2(I18n.t(:header, scope: "errors.template", model: object.model_name.human, count: object.errors.count))
        m.ul do |m|
          object.errors.full_messages.each do |message|
            m.li(message)
          end
        end
      end
    end

    private

    def base_receivers
      [form_builder, Markup::HtmlBuilder, helpers]
    end
  end
end
