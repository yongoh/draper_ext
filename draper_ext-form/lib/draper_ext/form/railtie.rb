module DraperExt
  module Form
    class Railtie < ::Rails::Railtie

      initializer "Add decoration feature" do
        ActionView::Helpers::FormBuilder.send(:include, Decoration.decoratable(:decorator){|definer|

          # #decorator_class
          definer.decorator_class_method do |form_builder|
            if form_builder.object.present?
              Decoration.const_fetch(form_builder.object.model_name.singular, :form_decorator)
            else
              Decoration.base_decorator_class
            end
          end

          # #decorate
          definer.decorate_method(:decorate)
        })
      end

      initializer "Extend view helper" do
        ActionView::Base.send(:include, FormHelper)
      end
    end
  end
end
