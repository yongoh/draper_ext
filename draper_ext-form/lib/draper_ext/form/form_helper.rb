module DraperExt
  module Form
    module FormHelper

      %i(form_for form_with).each do |method_name|

        # @param [Boolean] decoration ブロック引数に渡すフォームビルダーをデコレートするかどうか
        # @return [ActiveSupport::SafeBuffer] 検索フォーム要素
        define_method(method_name) do |*args, decoration: false, **options, &block|
          super(*args, options) do |f|
            block.call(decoration ? f.decorate : f)
          end
        end
      end
    end
  end
end
