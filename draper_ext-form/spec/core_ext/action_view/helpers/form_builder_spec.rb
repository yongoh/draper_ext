require 'rails_helper'

describe ActionView::Helpers::FormBuilder do
  let(:form_builder){ h.form_with(model: person){|f| return f } }
  let(:person){ create(:person) }

  describe "#decorate" do
    it "は、自身をデコレートしたフォームデコレータを返すこと" do
      expect(form_builder.decorate).to be_instance_of(PersonFormDecorator)
        .and have_attributes(
          form_builder: equal(form_builder),
          object: equal(person),
        )
    end
  end
end
