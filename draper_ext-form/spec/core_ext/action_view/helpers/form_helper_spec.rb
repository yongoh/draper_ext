require 'rails_helper'

describe ActionView::Helpers::FormHelper do
  let(:person){ create(:person) }

  describe "#form_for" do
    context "オプション`:decoration`に`false`を渡した場合 (default)" do
      it "は、ブロック引数にフォームビルダーを渡すこと" do
        expect{|block| h.form_with(model: person, &block) }.to yield_with_args(
          be_instance_of(ActionView::Helpers::FormBuilder).and have_attributes(object: equal(person))
        )
      end
    end

    context "オプション`:decoration`に`true`を渡した場合" do
      it "は、ブロック引数にフォームデコレータを渡すこと" do
        expect{|block| h.form_with(model: person, decoration: true, &block) }.to yield_with_args(
          be_instance_of(PersonFormDecorator).and have_attributes(object: equal(person))
        )
      end
    end

    subject do
      h.form_with(model: person) do
        h.concat h.content_tag(:div, "(´･ω･｀)", id: "shobon")
        h.concat h.content_tag(:span, "（＾ω＾）", class: "boon")
      end
    end

    it "は、フォーム要素を生成すること" do
      is_expected.to have_form("/people/#{person.id}", "post"){
        with_tag("div#shobon", text: "(´･ω･｀)")
        with_tag("span.boon", text: "（＾ω＾）")
      }
    end
  end

  describe "#form_with" do
    context "オプション`:decoration`に`false`を渡した場合 (default)" do
      it "は、ブロック引数にフォームビルダーを渡すこと" do
        expect{|block| h.form_with(url: h.people_path, &block) }.to yield_with_args(
          be_instance_of(ActionView::Helpers::FormBuilder).and have_attributes(object: be_nil)
        )
      end
    end

    context "オプション`:decoration`に`true`を渡した場合" do
      it "は、ブロック引数に基底デコレータを渡すこと" do
        expect{|block| h.form_with(url: h.people_path, decoration: true, &block) }.to yield_with_args(
          be_instance_of(Draper::Decorator).and have_attributes(object: be_a(ActionView::Helpers::FormBuilder))
        )
      end
    end
  end
end
