require 'rails_helper'

describe DraperExt::Form do
  let(:person){ create(:person) }

  describe "#error_explanation_about" do
    subject{ decorator.error_explanation_about }
    before{ person.valid? }

    let(:decorator){ double("Decorator", helpers: helper, object: person).extend(described_class) }

    context "バリデーションエラーが無いレコードを渡した場合" do
      it "は、エラーが無いことを通知するエラーメッセージ要素を生成すること" do
        is_expected.to have_tag("div.error_explanation"){
          with_tag("h2", text: "人物にエラーはありませんでした")
          with_tag("ul:empty")
        }
      end
    end

    context "バリデーションエラーがあるレコードを渡した場合" do
      let(:person){ build(:person, :invalid) }

      it "は、エラーの種類を通知するエラーメッセージ要素を生成すること" do
        is_expected.to have_tag("div.error_explanation"){
          with_tag("h2", text: "人物に2個のエラーが発生しました")
          with_tag("ul"){
            with_tag("li", count: 2)
            with_tag("li", text: "氏名を入力してください")
            with_tag("li", text: "身長は数値で入力してください")
          }
        }
      end
    end
  end
end
