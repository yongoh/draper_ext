$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "draper_ext/association/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "draper_ext-association"
  s.version     = DraperExt::Association::VERSION
  s.authors     = ["yongoh"]
  s.email       = ["a.yongoh@gmail.com"]
  s.homepage    = ""
  s.summary     = ""
  s.description = ""
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "route_link-restful"
  s.add_dependency 'enumeration_helper'
  s.add_dependency "draper_ext-decoration"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "database_cleaner"
  s.add_development_dependency "factory_bot_rails"
  s.add_development_dependency "rspec-html-matchers"
end
