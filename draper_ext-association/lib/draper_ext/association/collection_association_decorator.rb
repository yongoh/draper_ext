module DraperExt
  module Association
    class CollectionAssociationDecorator < Decorator
      include Enumerable
      include EnumerationHelper

      delegate :each, to: :target

      # @return [Draper::CollectionDecorator] 関連先のコレクションデコレータ
      def target(*args, &block)
        association.scope.decorate(*args, &block)
      end

      # @return [Integer] 関連先の件数
      def count
        association.scope.count
      end
    end
  end
end