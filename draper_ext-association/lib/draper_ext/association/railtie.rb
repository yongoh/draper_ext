module DraperExt
  module Association
    class Railtie < ::Rails::Railtie

      initializer "Add decoration feature" do
        mod1 = Decoration.decoratable(:decorator) do |definer|

          # #decorator_class
          definer.decorator_class_method do |association|
            Decoration.const_fetch(
              association.reflection.active_record.name,
              association.reflection.name,
              :association_decorator,
            ) do
              Association.const_get(Decoration.join(
                association.reflection.macro,
                (association.reflection.options.has_key?(:through) ? :through : nil),
                :association_decorator,
              ))
            end
          end

          # #decorate
          definer.decorate_method(:decorate)
        end

        mod2 = Module.new do

          # @note ブロックなしの場合は自動的に複数形Restfulのルーティングメソッド群を作成する
          def decorator_class(*args, &block)
            block ||= proc{ routes(resources: true) }
            super(*args, &block)
          end
        end

        ActiveRecord::Associations::Association.class_eval do
          include mod1
          include mod2
        end
      end

      initializer "draper_ext-association: configure route_link", after: "route_link-decoratable: configure route_link" do
        Decorator.class_eval do
          include RouteLink::Restful

          routes do |config|
            config.route_groups.delete_if{ true }
            config.link_params(_i18n_params: proc{|lb, d| {Object: proc{ d.human_association_name }, action: proc{ lb.route_params.human_action_name }} })

            # ラッパーメソッドを定義
            # FIXME: ポリモーフィック関連に対応していない
            config.wrap :collection do |route|
              route.route_params(_controller: proc{|lb, d| d.reflection.klass }, action: route.name)
              route.link_params(format: :action)
            end
          end
        end

        SingularAssociationDecorator.class_eval do
          routes do |config|

            # ラッパーメソッドを定義
            config.wrap :member do |route|
              route.route_params(_controller: proc{|lb, d| d.association.load_target }, action: route.name)
              route.link_params(format: :action)
            end

            # 複数形リソースのルーティンググループを定義
            config.route_group :resources do
              collection :index, {_query: :ransack_params}
              member :show
              member :edit
              collection :new, {_query: :record_params}
              member :destroy, {}, {_html_attributes: :destroy_link_html_attributes}
              collection :search, {_query: :ransack_params}
            end

            # 単数形リソースのルーティンググループを定義
            config.route_group :resource do
              collection :new, {_query: :record_params}
              collection :show
              collection :edit
              collection :destroy, {}, {_html_attributes: :destroy_link_html_attributes}
            end
          end
        end

        CollectionAssociationDecorator.class_eval do
          routes do |config|
            config.link_params(_i18n_params: proc{|lb, d| {count: proc{ d.count }} })

            # 複数形リソースのルーティンググループを定義
            config.route_group :resources do
              collection :index, {_query: :ransack_params}, {format: :action_with_count}
              collection :new, {_query: :record_params}
              collection :search, {_query: :ransack_params}
            end
          end
        end
      end
    end
  end
end
