module DraperExt
  module Association
    module EnumerationHelper

      # NOTE: `#banner`は draper_ext-model に依存している
      def enumerate(limit = 10, href: enumeration_index_path, **options, &block)
        block ||= :banner.to_proc
        h.enumerate_until(target, limit, href: href, **options, &block)
      end

      def enumeration_index_path
        try(:index_path)
      end
    end
  end
end
