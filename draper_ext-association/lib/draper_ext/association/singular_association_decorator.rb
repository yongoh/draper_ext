module DraperExt
  module Association
    class SingularAssociationDecorator < Decorator

      # @return [Draper::Decorator,nil] 関連先のデコレータ
      def target(*args, &block)
        t = association.load_target
        t.decorate(*args, &block) if t
      end
    end
  end
end