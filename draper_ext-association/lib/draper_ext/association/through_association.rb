module DraperExt
  module Association
    module ThroughAssociation

      protected

      # @return [Symbol] 中間モデルの属性値を示すURLパラメータキー
      def through_params_key
        [reverse_reflection.through_reflection.name, :attributes].join("_").to_sym
      end

      # @return [Hash,Array<Hash>] 中間モデルの属性値群
      def through_params
        reverse_reflection.through_reflection.collection? ? [owner_params] : owner_params
      end

      # @return [Symbol] 中間モデルの外部キー
      def owner_id_key
        ref = reverse_reflection.through_reflection || reverse_reflection
        if ref.collection?
          [ref.name.to_s.singularize, reflection.active_record_primary_key.pluralize].join("_").to_sym
        else
          super
        end
      end

      # @return [Hash] 中間モデルの属性値のうち自身との関連部分
      def owner_params
        ref = reverse_reflection.through_reflection || reverse_reflection
        if ref.collection?
          {owner_id_key => [association.owner.id]}
        else
          super
        end
      end

      # @return [Hash] 関連先の属性値群
      def target_attributes
        if !reversible?
          {}
        elsif reverse_reflection.through_reflection
          {through_params_key => through_params}
        else
          owner_params
        end
      end

      # @return [Hash] 検索用URLパラメータ
      def ransack_params(link_builder)
        reversible? ? super : {}
      end
    end
  end
end
