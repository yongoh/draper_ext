module DraperExt
  module Association
    module Humanizable

      # @return [String] 翻訳した関連名
      def human_association_name
        reflection.active_record.human_attribute_name(reflection.name)
      end
    end
  end
end
