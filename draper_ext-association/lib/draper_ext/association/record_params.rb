module DraperExt
  module Association

    # TODO: 他段階throughには未対応
    module RecordParams

      protected

      # @return [Symbol] 関連先のモデル名
      def target_attributes_key
        model_name.param_key.to_sym
      end

      # @return [Hash] 関連先の新規作成用URLパラメータ
      def record_params(link_builder)
        {target_attributes_key => target_attributes}
      end

      def target_attributes
        {}
      end
    end
  end
end
