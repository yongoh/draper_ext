module DraperExt
  module Association
    class HasOneThroughAssociationDecorator < HasOneAssociationDecorator
      include ThroughAssociation
    end
  end
end
