module DraperExt
  module Association
    class HasManyThroughAssociationDecorator < HasManyAssociationDecorator
      include ThroughAssociation
    end
  end
end
