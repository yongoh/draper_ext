module DraperExt
  module Association
    class Decorator < Draper::Decorator
      include Humanizable
      include RecordParams
      include RansackParams
      include Markup

      # @!attribute [rw] reverse_association_name
      #   @return [Symbol] 関連先から自身への関連（逆向き関連）の名前
      attr_accessor :reverse_association_name

      # @!attribute [r] association
      #   @return [ActiveRecord::Associations::Association] 「関連」オブジェクト
      alias_method :association, :object

      # @!attribute [r] reflection
      #   @return [ActiveRecord::Reflection::AssociationReflection] リフレクションオブジェクト
      delegate :reflection, to: :association

      # @param [Symbol] reverse 関連先から自身への関連（逆向き関連）の名前
      def initialize(*args, reverse: nil, **options, &block)
        super(*args, **options, &block)
        self.reverse_association_name = reverse.respond_to?(:to_sym) ? reverse.to_sym : reverse
      end

      def model_name
        association.owner.model_name
      end

      # @return [Boolean] 関連先が存在するかどうか
      def exists?
        association.scope.exists?
      end

      # @return [ActiveRecord::Reflection] 関連先から自身への関連
      def reverse_reflection
        association.klass.reflect_on_association(reverse_association_name)
      end

      # @return [Boolean] 関連先から自身への関連（逆向き関連）が設定されているかどうか
      def reversible?
        !!reverse_reflection
      end

      private

      def base_receivers
        [Markup::HtmlBuilder, helpers]
      end
    end
  end
end
