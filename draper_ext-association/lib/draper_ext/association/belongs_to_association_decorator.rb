module DraperExt
  module Association
    class BelongsToAssociationDecorator < SingularAssociationDecorator

      protected

      # @return [Symbol] 関連先の外部キー
      def owner_id_key
        key = [reverse_reflection.name.to_s.singularize, reflection.active_record.primary_key].join("_")
        (reverse_reflection.collection? ? key.pluralize : key).to_sym
      end

      # @return [Hash] 関連先の属性値群
      def target_attributes
        if !reversible?
          {}
        elsif reverse_reflection.collection?
          {owner_id_key => [association.owner.id]}
        else
          {owner_id_key => association.owner.id}
        end
      end

      # @return [Hash] 検索用URLパラメータ
      def ransack_params(link_builder)
        reversible? ? super : {}
      end
    end
  end
end
