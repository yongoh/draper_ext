module DraperExt
  module Association
    module RansackParams

      protected

      # @return [Symbol] 検索条件部分のキー
      def ransack_query_key
        :q
      end

      # @return [Symbol] 自身を関連先に持っていることを示す検索クエリキー
      def owner_id_query_key
        [reverse_reflection.name, reflection.klass.primary_key, "eq"].join("_").to_sym
      end

      # @return [Hash] 自身の関連先を検索するための条件
      def ransack_query
        {owner_id_query_key => association.owner.id}
      end

      # @return [Hash] 検索用URLパラメータ
      def ransack_params(link_builder)
        {ransack_query_key => ransack_query}
      end
    end
  end
end
