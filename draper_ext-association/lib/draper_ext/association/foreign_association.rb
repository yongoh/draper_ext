module DraperExt
  module Association
    module ForeignAssociation

      protected

      # @return [Symbol] 関連先の外部キー
      def owner_id_key
        reflection.foreign_key.to_sym
      end

      # @return [Hash] 関連先の属性値のうち自身との関連部分
      def owner_params
        {owner_id_key => association.owner.id}
      end

      # @return [Hash] 関連先の属性値群
      def target_attributes
        owner_params
      end
    end
  end
end
