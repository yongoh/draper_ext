module DraperExt
  module Association
    class HasManyAssociationDecorator < CollectionAssociationDecorator
      include ForeignAssociation

      protected

      # @return [Symbol] 自身を関連先に持っていることを示す検索クエリキー
      def owner_id_query_key
        if is_a?(ThroughAssociation)
          super
        else
          [owner_id_key, "eq"].join("_").to_sym
        end
      end
    end
  end
end
