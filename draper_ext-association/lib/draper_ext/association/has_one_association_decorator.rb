module DraperExt
  module Association
    class HasOneAssociationDecorator < SingularAssociationDecorator
      include ForeignAssociation
    end
  end
end
