require 'draper'
require 'markup'
require 'draper_ext/decoration'

require 'draper_ext/association/railtie'
require 'draper_ext/association/humanizable'
require 'draper_ext/association/record_params'
require 'draper_ext/association/ransack_params'
require 'draper_ext/association/enumeration_helper'
require 'draper_ext/association/decorator'
require 'draper_ext/association/foreign_association'
require 'draper_ext/association/through_association'
require 'draper_ext/association/singular_association_decorator'
require 'draper_ext/association/collection_association_decorator'
require 'draper_ext/association/belongs_to_association_decorator'
require 'draper_ext/association/belongs_to_polymorphic_association_decorator'
require 'draper_ext/association/has_one_association_decorator'
require 'draper_ext/association/has_one_through_association_decorator'
require 'draper_ext/association/has_many_association_decorator'
require 'draper_ext/association/has_many_through_association_decorator'

module DraperExt
  module Association
    include Decoration.decoratable(:association){|definer|

      # #association_class
      definer.decorator_class_method{|owner, name, *args| owner.object.association(name).decorator_class }

      # #association
      definer.decorate_method{|klass, owner, name, *args| klass.new(owner.object.association(name), *args) }

      # #association_markup
      definer.markup_method do |builder, assc|
        builder.default_html_attributes.merge!(
          class: %w(activerecord association),
          data: {
            model: assc.model_name.name,
            association: assc.reflection.name,
          }
        )
        builder.default_content_proc = proc do
          if assc.reflection.collection?
            Markup.h.concat(assc.enumerate)
          elsif assc.exists?
            Markup.h.concat(assc.target.banner)
          end
        end
      end

      # ::association, ::associations
      ClassMethods = definer.shortcut_definer_module
    }
  end
end
