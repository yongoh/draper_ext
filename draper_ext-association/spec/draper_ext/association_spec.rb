require 'rails_helper'

describe DraperExt::Association do
  let(:owner){ double("Owner", object: person, model_name: person.model_name).extend(described_class) }
  let(:person){ create(:person) }

  describe "#association" do
    it "は、渡した名前の「関連」デコレータを返すこと" do
      expect(owner.association(:wife)).to be_a(PersonWifeAssociationDecorator)
        .and have_attributes(object: equal(person.association(:wife)))
    end
  end

  describe "#association_markup.div" do
    context "単数関連で関連先がある場合" do
      let(:person){ create(:person, :with_parent) }

      it "は、関連先のバナー要素を内容に持つ関連要素を生成すること" do
        expect(owner.association_markup("parent").div).to have_tag(
          "div.activerecord.association",
          with: {
            "data-model" => "Person",
            "data-association" => "parent",
          },
        ){
          with_tag(".record_banner", count: 1)
          with_tag(".record_banner", with: {"data-model" => "Person", "data-record-id" => person.parent.id.to_s}, text: person.parent.name)
        }
      end
    end

    context "単数関連で関連先が無い場合" do
      it "は、空の関連要素を生成すること" do
        expect(owner.association_markup("parent").div).to have_tag(
          "div.activerecord.association:empty",
          with: {
            "data-model" => "Person",
            "data-association" => "parent",
          },
        )
      end
    end

    context "複数関連の場合" do
      let(:person){ create(:person, :with_children) }

      it "は、関連先のバナー要素群を内容に持つ関連要素を生成すること" do
        expect(owner.association_markup("children").div).to have_tag(
          "div.activerecord.association",
          with: {
            "data-model" => "Person",
            "data-association" => "children",
          },
        ){
          with_tag(".record_banner", count: person.children.size)
          person.children.each do |child|
            with_tag(".record_banner", with: {"data-model" => "Person", "data-record-id" => child.id.to_s}, text: child.name)
          end
        }
      end
    end
  end
end
