require 'rails_helper'

describe DraperExt::Association::HasManyAssociationDecorator do
  let(:decorator){ decorator_class.new(association, reverse: :parent) }
  let(:decorator_class){ Class.new(described_class) }
  let(:association){ person.association(:children) }
  let(:person){ create(:person) }

  describe "DraperExt::Association::RecordParams" do
    describe "#target_attributes_key" do
      it{ expect(decorator.send(:target_attributes_key)).to eq(:person) }
    end

    describe "#owner_id_key" do
      it{ expect(decorator.send(:owner_id_key)).to eq(:parent_id) }
    end

    describe "#record_params" do
      it{ expect(decorator.send(:record_params, nil)).to eq({person: {parent_id: person.id}}) }
    end
  end

  describe "DraperExt::Association::RansackParams" do
    describe "#owner_id_query_key" do
      it{ expect(decorator.send(:owner_id_query_key)).to eq(:parent_id_eq) }
    end

    describe "#ransack_params" do
      it{ expect(decorator.send(:ransack_params, nil)).to eq({q: {parent_id_eq: person.id}}) }
    end
  end

  describe "ルーティングメソッド群" do
    before do
      decorator_class.class_eval do
        routes resources: true
      end
    end

    describe "#index_path" do
      it{ expect(decorator.index_path).to eq("/people?q%5Bparent_id_eq%5D=#{person.id}") }
    end

    describe "#show_path" do
      it{ expect{ decorator.show_path }.to raise_error(NoMethodError, /undefined method `show_path'/) }
    end

    describe "#edit_path" do
      it{ expect{ decorator.edit_path }.to raise_error(NoMethodError, /undefined method `edit_path'/) }
    end

    describe "#new_path" do
      it{ expect(decorator.new_path).to eq("/people/new?person%5Bparent_id%5D=#{person.id}") }
    end

    describe "#destroy_path" do
      it{ expect{ decorator.destroy_path }.to raise_error(NoMethodError, /undefined method `destroy_path'/) }
    end

    describe "#search_path" do
      it{ expect(decorator.search_path).to eq("/people/search?q%5Bparent_id_eq%5D=#{person.id}") }
    end

    describe "#index_link" do
      let(:person){ create(:person, :with_children) }

      it "は、関連先の一覧へのリンクを生成すること" do
        expect(association.scope.count).to be == 3
        expect(decorator.index_link).to have_tag("a", with: {href: "/people?q%5Bparent_id_eq%5D=#{person.id}"}, text: "一覧 (3)")
      end
    end
  end
end
