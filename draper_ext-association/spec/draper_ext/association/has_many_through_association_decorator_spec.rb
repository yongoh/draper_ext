require 'rails_helper'

describe DraperExt::Association::HasManyThroughAssociationDecorator do
  let(:decorator){ decorator_class.new(association, reverse: :grandparent) }
  let(:decorator_class){ Class.new(described_class) }
  let(:association){ person.association(:grandchildren) }
  let(:person){ create(:person) }

  describe "DraperExt::Association::RecordParams" do
    describe "#target_attributes_key" do
      it{ expect(decorator.send(:target_attributes_key)).to eq(:person) }
    end

    describe "#through_params_key" do
      it{ expect(decorator.send(:through_params_key)).to eq(:parent_attributes) }
    end

    describe "#through_params" do
      it{ expect(decorator.send(:through_params)).to eq({parent_id: person.id}) }
    end

    describe "#owner_id_key" do
      it{ expect(decorator.send(:owner_id_key)).to eq(:parent_id) }
    end

    describe "#record_params" do
      it{ expect(decorator.send(:record_params, nil)).to eq({person: {parent_attributes: {parent_id: person.id}}}) }
    end
  end

  describe "DraperExt::Association::RansackParams" do
    describe "#owner_id_query_key" do
      it{ expect(decorator.send(:owner_id_query_key)).to eq(:grandparent_id_eq) }
    end

    describe "#ransack_params" do
      it{ expect(decorator.send(:ransack_params, nil)).to eq({q: {grandparent_id_eq: person.id}}) }
    end
  end

  describe "ルーティングメソッド群" do
    before do
      decorator_class.class_eval do
        routes resources: true
      end
    end

    describe "#index_path" do
      it{ expect(decorator.index_path).to eq("/people?q%5Bgrandparent_id_eq%5D=#{person.id}") }
    end

    describe "#show_path" do
      it{ expect{ decorator.show_path }.to raise_error(NoMethodError, /undefined method `show_path'/) }
    end

    describe "#edit_path" do
      it{ expect{ decorator.edit_path }.to raise_error(NoMethodError, /undefined method `edit_path'/) }
    end

    describe "#new_path" do
      it{ expect(decorator.new_path).to eq("/people/new?person%5Bparent_attributes%5D%5Bparent_id%5D=#{person.id}") }
    end

    describe "#destroy_path" do
      it{ expect{ decorator.destroy_path }.to raise_error(NoMethodError, /undefined method `destroy_path'/) }
    end

    describe "#search_path" do
      it{ expect(decorator.search_path).to eq("/people/search?q%5Bgrandparent_id_eq%5D=#{person.id}") }
    end

    describe "#index_link" do
      let(:person){ create(:person) }

      before do
        create_list(:person, 3, :with_children, parent: person)
      end

      it "は、関連先の一覧へのリンクを生成すること" do
        expect(association.scope.count).to be == 9
        expect(decorator.index_link).to have_tag("a", with: {href: "/people?q%5Bgrandparent_id_eq%5D=#{person.id}"}, text: "一覧 (9)")
      end
    end
  end
end
