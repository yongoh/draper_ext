require 'rails_helper'

describe DraperExt::Association::EnumerationHelper do
  let(:decorator){ association.decorate }
  let(:association){ person.association(:children) }
  let(:person){ create(:person, :with_children) }

  describe "#enumerate" do
    describe "yield with args" do
      it "は、コレクションの各要素のデコレータであること" do
        expect{|block| decorator.enumerate(&block) }.to yield_successive_args(
          *person.children.map{|child|
            be_a(Draper::Decorator).and have_attributes(object: eq(child))
          }
        )
      end
    end

    context "ブロックを渡した場合" do
      it "は、ブロックの戻り値を連結して返すこと" do
        sep = '<span class="enumeration separator"></span>'
        expect(decorator.enumerate{|d| d.object.name }).to eq(person.children.map(&:name).join(sep))
      end
    end

    context "ブロックを渡さない場合" do
      it "は、各モデルデコレータのバナー要素を列挙すること" do
        expect(decorator.enumerate).to have_tag(".record_banner", with: {"data-model" => "Person"}, count: 3)
      end
    end

    context "第1引数にコレクションの総数より小さい数値を渡した場合" do
      subject do
        decorator.enumerate(2)
      end

      context "ルーティング`:index`を設定していない場合" do
        let(:decorator){ association.decorate{} }

        it "は、渡した数だけ列挙し、リンクなし総数要素をつけて返すこと" do
          is_expected.to have_tag(".record_banner", with: {"data-model" => "Person"}, count: 2)
          is_expected.to have_tag(".enumeration.info", text: "...(2/3)", count: 1){
            without_tag("a")
          }
        end
      end

      context "ルーティング`:index`を設定している場合" do
        before do
          allow(decorator).to receive(:index_path).and_return("/foo/bar")
        end

        it "は、渡した数だけ列挙し、一覧へのリンク付き総数要素をつけて返すこと" do
          is_expected.to have_tag(".record_banner", with: {"data-model" => "Person"}, count: 2)
          is_expected.to have_tag(".enumeration.info", count: 1){
            with_tag("a", with: {href: "/foo/bar"}, text: "...(2/3)")
          }
        end
      end
    end
  end
end
