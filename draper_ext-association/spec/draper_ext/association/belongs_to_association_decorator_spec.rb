require 'rails_helper'

describe DraperExt::Association::BelongsToAssociationDecorator do
  let(:decorator_class){ Class.new(described_class) }

  context "逆向き関連を設定していない場合" do
    let(:decorator){ decorator_class.new(association) }
    let(:association){ person.association(:parent) }
    let(:person){ create(:person, :with_parent) }

    describe "ルーティングメソッド群" do
      before do
        decorator_class.class_eval do
          routes resources: true
        end
      end

      describe "#index_path" do
        it{ expect(decorator.index_path).to eq("/people") }
      end

      describe "#new_path" do
        it{ expect(decorator.new_path).to eq("/people/new") }
      end
    end
  end

  context "逆向き関連が複数関連の場合" do
    let(:decorator){ decorator_class.new(association, reverse: :children) }
    let(:association){ person.association(:parent) }
    let(:person){ create(:person, :with_parent) }

    describe "DraperExt::Association::RecordParams" do
      describe "#target_attributes_key" do
        it{ expect(decorator.send(:target_attributes_key)).to eq(:person) }
      end

      describe "#owner_id_key" do
        it{ expect(decorator.send(:owner_id_key)).to eq(:child_ids) }
      end

      describe "#target_attributes" do
        it{ expect(decorator.send(:target_attributes)).to eq({child_ids: [person.id]}) }
      end

      describe "#record_params" do
        it{ expect(decorator.send(:record_params, nil)).to eq({person: {child_ids: [person.id]}}) }
      end
    end

    describe "DraperExt::Association::RansackParams" do
      describe "#owner_id_query_key" do
        it{ expect(decorator.send(:owner_id_query_key)).to eq(:children_id_eq) }
      end

      describe "#ransack_params" do
        it{ expect(decorator.send(:ransack_params, nil)).to eq({q: {children_id_eq: person.id}}) }
      end
    end

    describe "ルーティングメソッド群" do
      before do
        decorator_class.class_eval do
          routes resources: true
        end
      end

      describe "#index_path" do
        it{ expect(decorator.index_path).to eq("/people?q%5Bchildren_id_eq%5D=#{person.id}") }
      end

      describe "#show_path" do
        it{ expect(decorator.show_path).to eq("/people/#{person.parent_id}") }
      end

      describe "#edit_path" do
        it{ expect(decorator.edit_path).to eq("/people/#{person.parent_id}/edit") }
      end

      describe "#new_path" do
        it{ expect(decorator.new_path).to eq("/people/new?person%5Bchild_ids%5D%5B%5D=#{person.id}") }
      end

      describe "#destroy_path" do
        it{ expect(decorator.destroy_path).to eq("/people/#{person.parent_id}") }
      end

      describe "#search_path" do
        it{ expect(decorator.search_path).to eq("/people/search?q%5Bchildren_id_eq%5D=#{person.id}") }
      end

      describe "#index_link" do
        it "は、関連先の一覧へのリンクを生成すること" do
          expect(decorator.index_link).to have_tag("a", with: {href: "/people?q%5Bchildren_id_eq%5D=#{person.id}"}, text: "一覧")
        end
      end

      describe "#destroy_link" do
        it "は、関連先の削除アクションへのリンクを生成すること" do
          expect(decorator.destroy_link).to have_tag("a[data-confirm]", with: {href: "/people/#{person.parent_id}", "data-method" => "delete"}, text: "削除")
        end
      end
    end
  end

  context "逆向き関連が単数関連の場合" do
    let(:decorator){ decorator_class.new(association, reverse: :husband) }
    let(:association){ person.association(:wife) }
    let(:person){ create(:person, :with_wife) }

    describe "DraperExt::Association::RecordParams" do
      describe "#target_attributes_key" do
        it{ expect(decorator.send(:target_attributes_key)).to eq(:person) }
      end

      describe "#owner_id_key" do
        it{ expect(decorator.send(:owner_id_key)).to eq(:husband_id) }
      end

      describe "#target_attributes" do
        it{ expect(decorator.send(:target_attributes)).to eq({husband_id: person.id}) }
      end

      describe "#record_params" do
        it{ expect(decorator.send(:record_params, nil)).to eq({person: {husband_id: person.id}}) }
      end
    end

    describe "DraperExt::Association::RansackParams" do
      describe "#owner_id_query_key" do
        it{ expect(decorator.send(:owner_id_query_key)).to eq(:husband_id_eq) }
      end

      describe "#ransack_params" do
        it{ expect(decorator.send(:ransack_params, nil)).to eq({q: {husband_id_eq: person.id}}) }
      end
    end

    describe "ルーティングメソッド群" do
      before do
        decorator_class.class_eval do
          routes resources: true
        end
      end

      describe "#index_path" do
        it{ expect(decorator.index_path).to eq("/people?q%5Bhusband_id_eq%5D=#{person.id}") }
      end

      describe "#show_path" do
        it{ expect(decorator.show_path).to eq("/people/#{person.wife.id}") }
      end

      describe "#edit_path" do
        it{ expect(decorator.edit_path).to eq("/people/#{person.wife.id}/edit") }
      end

      describe "#new_path" do
        it{ expect(decorator.new_path).to eq("/people/new?person%5Bhusband_id%5D=#{person.id}") }
      end

      describe "#destroy_path" do
        it{ expect(decorator.destroy_path).to eq("/people/#{person.wife.id}") }
      end

      describe "#search_path" do
        it{ expect(decorator.search_path).to eq("/people/search?q%5Bhusband_id_eq%5D=#{person.id}") }
      end

      describe "#index_link" do
        it "は、関連先の一覧へのリンクを生成すること" do
          expect(decorator.index_link).to have_tag("a", with: {href: "/people?q%5Bhusband_id_eq%5D=#{person.id}"}, text: "一覧")
        end
      end

      describe "#destroy_link" do
        it "は、関連先の削除アクションへのリンクを生成すること" do
          expect(decorator.destroy_link).to have_tag("a[data-confirm]", with: {href: "/people/#{person.wife.id}", "data-method" => "delete"}, text: "削除")
        end
      end
    end
  end
end
