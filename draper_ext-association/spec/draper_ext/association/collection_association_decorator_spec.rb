require 'rails_helper'

describe DraperExt::Association::CollectionAssociationDecorator do
  let(:decorator){ described_class.new(association) }
  let(:association){ person.association(:children) }
  let(:person){ create(:person, :with_children) }

  describe "#target" do
    it "は、関連先のコレクションデコレータを返すこと" do
      expect(decorator.target).to be_a(Draper::CollectionDecorator).and have_attributes(object: person.children)
    end
  end

  describe "#exists?" do
    context "関連先が存在する場合" do
      it{ expect(decorator).to be_exists }
    end

    context "関連先が存在しない場合" do
      let(:person){ create(:person) }
      it{ expect(decorator).not_to be_exists }
    end
  end

  describe "#each" do
    let(:expected){
      person.children.map do |child|
        be_a(Draper::Decorator).and(have_attributes(object: child))
      end
    }

    context "ブロックを渡さない場合" do
      it "は、デコレートした関連先を含む`Enumerator`を返すこと" do
        expect(decorator.each).to be_instance_of(Enumerator).and match(expected)
      end
    end

    context "ブロックを渡した場合" do
      it "は、ブロック引数にデコレートした関連先を渡すこと" do
        expect{|block| decorator.each(&block) }.to yield_successive_args(*expected)
      end
    end
  end

  describe "#count" do
    it "は、関連先の件数を返すこと" do
      expect(decorator.count).to eq(person.children.size)
    end
  end
end
