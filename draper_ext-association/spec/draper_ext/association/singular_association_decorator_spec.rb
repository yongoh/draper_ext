require 'rails_helper'

describe DraperExt::Association::SingularAssociationDecorator do
  let(:decorator){ described_class.new(association) }
  let(:association){ person.association(:parent) }
  let(:person){ create(:person, :with_parent) }

  describe "#target" do
    context "関連先が存在する場合" do
      it "は、関連先のデコレータを返すこと" do
        expect(decorator.target).to be_a(Draper::Decorator).and have_attributes(object: person.parent)
      end
    end

    context "関連先が存在しない場合" do
      let(:person){ create(:person) }
      it{ expect(decorator.target).to be_nil }
    end

    context "関連先を後から設定した場合" do
      before do
        person.parent_id = parent.id
      end

      let(:person){ create(:person) }
      let(:parent){ create(:person) }

      it "は、関連先のデコレータを返すこと" do
        expect(decorator.target).to be_a(Draper::Decorator).and have_attributes(object: person.parent)
      end
    end
  end

  describe "#exists?" do
    context "関連先が存在する場合" do
      it{ expect(decorator).to be_exists }
    end

    context "関連先が存在しない場合" do
      let(:person){ create(:person) }
      it{ expect(decorator).not_to be_exists }
    end
  end
end
