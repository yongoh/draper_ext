require 'rails_helper'

describe DraperExt::Association::HasManyThroughAssociationDecorator do
  let(:decorator){ decorator_class.new(association, reverse: :friends) }
  let(:decorator_class){ Class.new(described_class) }
  let(:association){ person.association(:friends) }
  let(:person){ create(:person) }

  describe "DraperExt::Association::RecordParams" do
    describe "#target_attributes_key" do
      it{ expect(decorator.send(:target_attributes_key)).to eq(:person) }
    end

    describe "#owner_id_key" do
      it{ expect(decorator.send(:owner_id_key)).to eq(:friend_ids) }
    end

    describe "#record_params" do
      it{ expect(decorator.send(:record_params, nil)).to eq({person: {friend_ids: [person.id]}}) }
    end
  end

  describe "DraperExt::Association::RansackParams" do
    describe "#owner_id_query_key" do
      it{ expect(decorator.send(:owner_id_query_key)).to eq(:friends_id_eq) }
    end

    describe "#ransack_params" do
      it{ expect(decorator.send(:ransack_params, nil)).to eq({q: {friends_id_eq: person.id}}) }
    end
  end

  describe "ルーティングメソッド群" do
    before do
      decorator_class.class_eval do
        routes resources: true
      end
    end

    describe "#index_path" do
      it{ expect(decorator.index_path).to eq("/people?q%5Bfriends_id_eq%5D=#{person.id}") }
    end

    describe "#show_path" do
      it{ expect{ decorator.show_path }.to raise_error(NoMethodError, /undefined method `show_path'/) }
    end

    describe "#edit_path" do
      it{ expect{ decorator.edit_path }.to raise_error(NoMethodError, /undefined method `edit_path'/) }
    end

    describe "#new_path" do
      it{ expect(decorator.new_path).to eq("/people/new?person%5Bfriend_ids%5D%5B%5D=#{person.id}") }
    end

    describe "#destroy_path" do
      it{ expect{ decorator.destroy_path }.to raise_error(NoMethodError, /undefined method `destroy_path'/) }
    end

    describe "#search_path" do
      it{ expect(decorator.search_path).to eq("/people/search?q%5Bfriends_id_eq%5D=#{person.id}") }
    end

    describe "#index_link" do
      let(:person){ create(:person, :with_friends) }

      it "は、関連先の一覧へのリンクを生成すること" do
        expect(association.scope.count).to be == 3
        expect(decorator.index_link).to have_tag("a", with: {href: "/people?q%5Bfriends_id_eq%5D=#{person.id}"}, text: "一覧 (3)")
      end
    end
  end
end
