require 'rails_helper'

describe DraperExt::Association::HasOneThroughAssociationDecorator do
  let(:decorator){ decorator_class.new(association, reverse: :grandchildren) }
  let(:decorator_class){ Class.new(described_class) }
  let(:association){ person.association(:grandparent) }
  let(:person){ create(:person, :with_grandparent) }

  describe "DraperExt::Association::RecordParams" do
    describe "#target_attributes_key" do
      it{ expect(decorator.send(:target_attributes_key)).to eq(:person) }
    end

    describe "#through_params_key" do
      it{ expect(decorator.send(:through_params_key)).to eq(:children_attributes) }
    end

    describe "#through_params" do
      it{ expect(decorator.send(:through_params)).to eq([{child_ids: [person.id]}]) }
    end

    describe "#owner_id_key" do
      it{ expect(decorator.send(:owner_id_key)).to eq(:child_ids) }
    end

    describe "#record_params" do
      it{ expect(decorator.send(:record_params, nil)).to eq({person: {children_attributes: [{child_ids: [person.id]}]}}) }
    end
  end

  describe "DraperExt::Association::RansackParams" do
    describe "#owner_id_query_key" do
      it{ expect(decorator.send(:owner_id_query_key)).to eq(:grandchildren_id_eq) }
    end

    describe "#ransack_params" do
      it{ expect(decorator.send(:ransack_params, nil)).to eq({q: {grandchildren_id_eq: person.id}}) }
    end
  end

  describe "ルーティングメソッド群" do
    before do
      decorator_class.class_eval do
        routes resources: true
      end
    end

    describe "#index_path" do
      it{ expect(decorator.index_path).to eq("/people?q%5Bgrandchildren_id_eq%5D=#{person.id}") }
    end

    describe "#show_path" do
      it{ expect(decorator.show_path).to eq("/people/#{person.grandparent.id}") }
    end

    describe "#edit_path" do
      it{ expect(decorator.edit_path).to eq("/people/#{person.grandparent.id}/edit") }
    end

    describe "#new_path" do
      it{ expect(decorator.new_path).to eq("/people/new?person%5Bchildren_attributes%5D%5B%5D%5Bchild_ids%5D%5B%5D=#{person.id}") }
    end

    describe "#destroy_path" do
      it{ expect(decorator.destroy_path).to eq("/people/#{person.grandparent.id}") }
    end

    describe "#search_path" do
      it{ expect(decorator.search_path).to eq("/people/search?q%5Bgrandchildren_id_eq%5D=#{person.id}") }
    end

    describe "#index_link" do
      it "は、関連先の一覧へのリンクを生成すること" do
        expect(decorator.index_link).to have_tag("a", with: {href: "/people?q%5Bgrandchildren_id_eq%5D=#{person.id}"}, text: "一覧")
      end
    end

    describe "#destroy_link" do
      it "は、関連先の削除アクションへのリンクを生成すること" do
        expect(decorator.destroy_link).to have_tag("a[data-confirm]", with: {href: "/people/#{person.grandparent.id}", "data-method" => "delete"}, text: "削除")
      end
    end
  end
end
