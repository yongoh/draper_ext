require 'rails_helper'

describe DraperExt::Association::Humanizable do
  let(:owner){ double("AssociationDecorator", reflection: reflection).extend(described_class) }
  let(:reflection){ double("Reflection", active_record: Person, klass: Person) }

  describe "#human_association_name" do
    context "訳文`activerecord.attributes.{active_record}.{klass}`がある場合" do
      before do
        allow(reflection).to receive(:name).and_return(:parent)
      end

      it "は、その訳文を返すこと" do
        expect(owner.human_association_name).to eq("親")
      end
    end

    context "訳文`activerecord.attributes.{active_record}.{klass}`が無い場合" do
      before do
        allow(reflection).to receive(:name).and_return(:undefined_key)
      end

      it "は、英文化した関連名を返すこと" do
        expect(owner.human_association_name).to eq("Undefined key")
      end
    end
  end
end
