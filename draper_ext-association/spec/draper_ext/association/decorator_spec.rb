require 'rails_helper'

describe DraperExt::Association::Decorator do
  let(:decorator){ decorator_class.new(association) }
  let(:decorator_class){ described_class }
  let(:association){ create(:person).association(:parent) }

  describe "#initialize" do
    context "オプション`:reverse`に`nil`を渡した場合 (default)" do
      it "は、`#reverse_association_name`に`nil`をセットすること" do
        expect(decorator.reverse_association_name).to be_nil
      end
    end

    context "オプション`:reverse`に文字列を渡した場合" do
      let(:decorator){ decorator_class.new(association, reverse: "hoge") }

      it "は、`#reverse_association_name`に文字列をSymbol化したものをセットすること" do
        expect(decorator.reverse_association_name).to eq(:hoge)
      end
    end
  end

  describe "#association" do
    it{ expect(decorator.association).to equal(association) }
  end

  describe "#reflection (delegate to `@association`)" do
    it{ expect(decorator.reflection).to be_a(ActiveRecord::Reflection::AssociationReflection) }
  end

  describe "#reverse_reflection" do
    subject do
      decorator.reverse_reflection
    end

    context "逆向き関連名をセットしていない場合" do
      it{ is_expected.to be_nil }
    end

    context "逆向き関連名をセットしている場合" do
      let(:decorator){ decorator_class.new(association, reverse: :children) }

      it "は、関連先から自身への関連リフレクションオブジェクトを返すこと" do
        is_expected.to be_a(ActiveRecord::Reflection::AssociationReflection)
          .and have_attributes(
            name: :children,
            macro: :has_many,
            active_record: Person,
            klass: Person,
          )
      end
    end

    context "関連先モデルに存在しない関連名をセットしている場合" do
      let(:decorator){ decorator_class.new(association, reverse: :undefined) }
      it{ is_expected.to be_nil }
    end
  end
end
