require 'rails_helper'

describe ActiveRecord::Associations::Association do
  let(:person){ create(:person) }

  describe "#decorate" do
    shared_examples_for "return decorator" do
      it "は、関連の種類に応じたデコレータのインスタンスを返すこと" do
        expect(association.decorate).to be_a(decorator_class)
          .and have_attributes(object: equal(association))
      end
    end

    context "`belongs_to`関連オブジェクトの場合" do
      let(:association){ person.association(:parent) }
      let(:decorator_class){ DraperExt::Association::BelongsToAssociationDecorator }
      it_behaves_like "return decorator"
    end

    context "`has_one`関連オブジェクトの場合" do
      let(:association){ person.association(:husband) }
      let(:decorator_class){ DraperExt::Association::HasOneAssociationDecorator }
      it_behaves_like "return decorator"
    end

    context "`has_one through`関連オブジェクトの場合" do
      let(:association){ person.association(:grandparent) }
      let(:decorator_class){ DraperExt::Association::HasOneThroughAssociationDecorator }
      it_behaves_like "return decorator"
    end

    context "`has_many`関連オブジェクトの場合" do
      let(:association){ person.association(:children) }
      let(:decorator_class){ DraperExt::Association::HasManyAssociationDecorator }
      it_behaves_like "return decorator"
    end

    context "`has_many through`関連オブジェクトの場合" do
      let(:association){ person.association(:grandchildren) }
      let(:decorator_class){ DraperExt::Association::HasManyThroughAssociationDecorator }
      it_behaves_like "return decorator"
    end

    context "`has_and_belongs_to_many`関連オブジェクトの場合" do
      let(:association){ person.association(:friends) }
      let(:decorator_class){ DraperExt::Association::HasManyThroughAssociationDecorator }
      it_behaves_like "return decorator"
    end

    context "自身に対応する名前のクラス（定数）がある場合" do
      let(:association){ person.association(:wife) }
      let(:decorator_class){ PersonWifeAssociationDecorator }
      it_behaves_like "return decorator"
    end

    context "ブロックを" do
      subject do
        decorator.public_methods
      end

      let(:association){ person.association(:parent) }

      context "渡さない場合" do
        let(:decorator){ association.decorate }

        it "は、複数形リソースのルーティングメソッド群が定義されたデコレータを返すこと" do
          is_expected.to include(
            :index_path, :index_url, :index_name, :index_link,
            :new_path, :new_url, :new_name, :new_link,
            :show_path, :show_url, :show_name, :show_link,
            :edit_path, :edit_url, :edit_name, :edit_link,
            :search_path, :search_url, :search_name, :search_link,
            :destroy_path, :destroy_url, :destroy_name, :destroy_link,
          )
        end
      end

      context "渡して中で`#routes`を使わなかった場合" do
        let(:decorator){ association.decorate{} }

        it "は、ルーティングメソッド群が定義されていないデコレータを返すこと" do
          is_expected.not_to include(
            :index_path, :index_url, :index_name, :index_link,
            :show_path, :show_url, :show_name, :show_link,
            :new_path, :new_url, :new_name, :new_link,
            :edit_path, :edit_url, :edit_name, :edit_link,
            :search_path, :search_url, :search_name, :search_link,
            :destroy_path, :destroy_url, :destroy_name, :destroy_link,
          )
        end
      end
    end
  end
end
