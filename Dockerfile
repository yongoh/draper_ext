FROM yongoh/rails:6
ENV LANG C.UTF-8
ARG APP_NAME=draper_ext
ARG APP_HOME=/product

WORKDIR /tmp

ADD Gemfile Gemfile
ADD Gemfile.lock Gemfile.lock
ADD $APP_NAME.gemspec $APP_NAME.gemspec
ADD lib/$APP_NAME/version.rb lib/$APP_NAME/version.rb

ADD $APP_NAME-decoration/Gemfile $APP_NAME-decoration/Gemfile
ADD $APP_NAME-decoration/Gemfile.lock $APP_NAME-decoration/Gemfile.lock
ADD $APP_NAME-decoration/$APP_NAME-decoration.gemspec $APP_NAME-decoration/$APP_NAME-decoration.gemspec
ADD $APP_NAME-decoration/lib/$APP_NAME/decoration/version.rb $APP_NAME-decoration/lib/$APP_NAME/decoration/version.rb

ADD $APP_NAME-model/Gemfile $APP_NAME-model/Gemfile
ADD $APP_NAME-model/Gemfile.lock $APP_NAME-model/Gemfile.lock
ADD $APP_NAME-model/$APP_NAME-model.gemspec $APP_NAME-model/$APP_NAME-model.gemspec
ADD $APP_NAME-model/lib/$APP_NAME/model/version.rb $APP_NAME-model/lib/$APP_NAME/model/version.rb

ADD $APP_NAME-form/Gemfile $APP_NAME-form/Gemfile
ADD $APP_NAME-form/Gemfile.lock $APP_NAME-form/Gemfile.lock
ADD $APP_NAME-form/$APP_NAME-form.gemspec $APP_NAME-form/$APP_NAME-form.gemspec
ADD $APP_NAME-form/lib/$APP_NAME/form/version.rb $APP_NAME-form/lib/$APP_NAME/form/version.rb

ADD $APP_NAME-attribute/Gemfile $APP_NAME-attribute/Gemfile
ADD $APP_NAME-attribute/Gemfile.lock $APP_NAME-attribute/Gemfile.lock
ADD $APP_NAME-attribute/$APP_NAME-attribute.gemspec $APP_NAME-attribute/$APP_NAME-attribute.gemspec
ADD $APP_NAME-attribute/lib/$APP_NAME/attribute/version.rb $APP_NAME-attribute/lib/$APP_NAME/attribute/version.rb

ADD $APP_NAME-field/Gemfile $APP_NAME-field/Gemfile
ADD $APP_NAME-field/Gemfile.lock $APP_NAME-field/Gemfile.lock
ADD $APP_NAME-field/$APP_NAME-field.gemspec $APP_NAME-field/$APP_NAME-field.gemspec
ADD $APP_NAME-field/lib/$APP_NAME/field/version.rb $APP_NAME-field/lib/$APP_NAME/field/version.rb

ADD $APP_NAME-association/Gemfile $APP_NAME-association/Gemfile
ADD $APP_NAME-association/Gemfile.lock $APP_NAME-association/Gemfile.lock
ADD $APP_NAME-association/$APP_NAME-association.gemspec $APP_NAME-association/$APP_NAME-association.gemspec
ADD $APP_NAME-association/lib/$APP_NAME/association/version.rb $APP_NAME-association/lib/$APP_NAME/association/version.rb

ADD $APP_NAME-association_form/Gemfile $APP_NAME-association_form/Gemfile
ADD $APP_NAME-association_form/Gemfile.lock $APP_NAME-association_form/Gemfile.lock
ADD $APP_NAME-association_form/$APP_NAME-association_form.gemspec $APP_NAME-association_form/$APP_NAME-association_form.gemspec
ADD $APP_NAME-association_form/lib/$APP_NAME/association_form/version.rb $APP_NAME-association_form/lib/$APP_NAME/association_form/version.rb

ADD $APP_NAME-ransack/Gemfile $APP_NAME-ransack/Gemfile
ADD $APP_NAME-ransack/Gemfile.lock $APP_NAME-ransack/Gemfile.lock
ADD $APP_NAME-ransack/$APP_NAME-ransack.gemspec $APP_NAME-ransack/$APP_NAME-ransack.gemspec
ADD $APP_NAME-ransack/lib/$APP_NAME/ransack/version.rb $APP_NAME-ransack/lib/$APP_NAME/ransack/version.rb

RUN bundle install

RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME
ADD . $APP_HOME
