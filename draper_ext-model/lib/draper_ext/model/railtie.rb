module DraperExt
  module Model
    class Railtie < ::Rails::Railtie

      initializer "Add decoration feature to ActiveRecord::Base" do
        ActiveRecord::Base.send(:include, Decoration.decoratable(:decorator){|definer|

          # #decorator_class
          definer.decorator_class_method do |record, type: :model, **options|
            Decoration.const_fetch(record.model_name.singular, type, :decorator)
          end

          # #decorate
          definer.decorate_method(:decorate){|klass, record, **options| klass.new(record, options.except(:type)) }
        })
      end

      initializer "Add decoration feature to ActiveRecord::Relation" do
        ActiveRecord::Relation.send(:include, Decoration.decoratable(:decorator){|definer|

          # #decorator_class
          definer.decorator_class_method do |relation|
            Decoration.const_fetch(relation.klass.model_name.plural, :decorator){ Draper::CollectionDecorator }
          end

          # #decorate
          definer.decorate_method(:decorate) do |decorator_class, relation, type: :table, **options|
            unless options.has_key?(:with)
              options.merge!(with: Decoration.const_fetch(relation.klass.model_name.singular, type, :decorator))
            end
            decorator_class.new(relation, options)
          end
        })
      end

      initializer "Add enumeration feature" do
        Draper::CollectionDecorator.send(:include, EnumerationHelper)
      end

      initializer "draper_ext-model: configure route_link", after: "route_link-decoratable: configure route_link" do
        Model.class_eval do
          include RouteLink::Restful

          included do
            extend RouteLink
          end
        end
      end
    end
  end
end
