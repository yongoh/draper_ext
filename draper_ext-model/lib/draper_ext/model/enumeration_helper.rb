module DraperExt
  module Model
    module EnumerationHelper

      def enumerate(limit = 10, href: enumeration_index_path, **options, &block)
        block ||= :banner.to_proc
        h.enumerate_until(object, limit, href: href, **options) do |record|
          block.call(decorate_item(record))
        end
      end

      def enumeration_index_path
        h.polymorphic_path(object.klass) if object.respond_to?(:klass)
      end
    end
  end
end
