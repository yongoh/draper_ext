require 'draper_ext/decoration'

require 'draper_ext/model/railtie'
require 'draper_ext/model/enumeration_helper'

module DraperExt
  module Model
    extend ActiveSupport::Concern
    include Markup

    module ClassMethods
    end

    delegate :model_name, to: :object

    private

    def base_receivers
      [Markup::HtmlBuilder, helpers]
    end
  end
end
