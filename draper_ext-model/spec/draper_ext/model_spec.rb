require 'rails_helper'

describe DraperExt::Model do
  let(:decorator){ decorator_class.new(person) }
  let(:decorator_class){ Class.new(Draper::Decorator).include(described_class) }
  let(:person){ create(:person) }

  describe "ルーティングメソッド群" do
    before do
      decorator_class.class_eval do
        routes resources: true
      end
    end

    describe "#index_link" do
      it{ expect(decorator.index_link).to have_tag("a", with: {href: "/people"}, text: "一覧") }
    end

    describe "#show_link" do
      it{ expect(decorator.show_link).to have_tag("a", with: {href: "/people/#{person.id}"}, text: "詳細") }
    end

    describe "#new_link" do
      it{ expect(decorator.new_link).to have_tag("a", with: {href: "/people/new"}, text: "新規作成") }
    end

    describe "#edit_link" do
      it{ expect(decorator.edit_link).to have_tag("a", with: {href: "/people/#{person.id}/edit"}, text: "編集") }
    end

    describe "#destroy_link" do
      it{ expect(decorator.destroy_link).to have_tag("a[data-confirm]", with: {href: "/people/#{person.id}", "data-method" => "delete"}, text: "削除") }
    end

    describe "#search_link" do
      it{ expect(decorator.search_link).to have_tag("a", with: {href: "/people/search"}, text: "検索") }
    end
  end
end
