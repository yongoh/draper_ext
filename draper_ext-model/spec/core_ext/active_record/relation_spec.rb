require 'rails_helper'

describe ActiveRecord::Relation do
  let(:people){ Person.all }

  before do
    create_list(:person, 3)
  end

  describe "#decorate" do
    shared_examples_for "result collection decorator" do
      it "は、自身をデコレートしたコレクションデコレータを返すこと" do
        expect(result).to be_instance_of(PeopleDecorator)
          .and have_attributes(object: equal(people))
      end
    end

    context "引数なしの場合" do
      let(:result){ people.decorate }

      it_behaves_like "result collection decorator"

      it "は、コレクションに含まれる各レコードをテーブル用デコレータクラスでデコレートすること" do
        expect{|block| result.each(&block) }.to yield_successive_args(
          be_instance_of(PersonTableDecorator).and(have_attributes(object: equal(people[0]))),
          be_instance_of(PersonTableDecorator).and(have_attributes(object: equal(people[1]))),
          be_instance_of(PersonTableDecorator).and(have_attributes(object: equal(people[2]))),
        )
      end
    end

    context "オプション`:with`にデコレータクラスを渡した場合" do
      let(:result){ people.decorate(with: decorator_class) }
      let(:decorator_class){ Class.new(Draper::Decorator) }

      it_behaves_like "result collection decorator"

      it "は、コレクションに含まれる各レコードをそのクラスでデコレートすること" do
        expect{|block| result.each(&block) }.to yield_successive_args(
          be_instance_of(decorator_class).and(have_attributes(object: equal(people[0]))),
          be_instance_of(decorator_class).and(have_attributes(object: equal(people[1]))),
          be_instance_of(decorator_class).and(have_attributes(object: equal(people[2]))),
        )
      end
    end

    context "第1引数に各レコードデコレータの種類（使う文脈）を渡した場合" do
      let(:result){ people.decorate(type: :model) }
      let(:decorator_class){ Class.new(Draper::Decorator) }

      it_behaves_like "result collection decorator"

      it "は、コレクションに含まれる各レコードをその種類のデコレータクラスでデコレートすること" do
        expect{|block| result.each(&block) }.to yield_successive_args(
          be_instance_of(PersonModelDecorator).and(have_attributes(object: equal(people[0]))),
          be_instance_of(PersonModelDecorator).and(have_attributes(object: equal(people[1]))),
          be_instance_of(PersonModelDecorator).and(have_attributes(object: equal(people[2]))),
        )
      end
    end
  end
end
