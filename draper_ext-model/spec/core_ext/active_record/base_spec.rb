require 'rails_helper'

describe ActiveRecord::Base do
  let(:person){ create(:person) }

  describe "#decorate" do
    context "引数なしの場合" do
      it "は、自身をデコレートしたモデルデコレータを返すこと" do
        expect(person.decorate).to be_instance_of(PersonModelDecorator)
          .and have_attributes(object: equal(person))
      end
    end

    context "デコレータの種類（使う文脈）を渡した場合" do
      context "渡した種類のクラス（定数）が存在する場合" do
        it "は、渡した種類のデコレータを返すこと" do
          expect(person.decorate(type: :table)).to be_instance_of(PersonTableDecorator)
            .and have_attributes(object: equal(person))
        end
      end

      context "渡した種類のクラス（定数）が存在しない場合" do
        it "は、基底デコレータを返すこと" do
          expect(person.decorate(type: :hoge)).to be_instance_of(Draper::Decorator)
            .and have_attributes(object: equal(person))
        end
      end
    end
  end
end
