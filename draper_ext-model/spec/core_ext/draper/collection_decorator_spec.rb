require 'rails_helper'

describe Draper::CollectionDecorator do
  let(:decorator){ described_class.new(collection) }
  let(:collection){ Person.all }

  before do
    create_list(:person, 5)
  end

  describe "#enumerate" do
    describe "yield with args" do
      context "インスタンス化時に各要素のデコレータクラスを渡していない場合" do
        it "は、コレクションの各要素のデコレータであること" do
          expect{|block| decorator.enumerate(&block) }.to yield_successive_args(
            *collection.map{|person|
              be_instance_of(PersonModelDecorator).and have_attributes(object: eq(person))
            }
          )
        end
      end

      context "インスタンス化時に各要素のデコレータクラスを渡していた場合" do
        let(:decorator){ described_class.new(collection, with: PersonTableDecorator) }

        it "は、コレクションの各要素のデコレータであること" do
          expect{|block| decorator.enumerate(&block) }.to yield_successive_args(
            *collection.map{|person|
              be_instance_of(PersonTableDecorator).and have_attributes(object: eq(person))
            }
          )
        end
      end
    end

    context "ブロックを渡した場合" do
      it "は、ブロックの戻り値を連結して返すこと" do
        sep = '<span class="enumeration separator"></span>'
        expect(decorator.enumerate{|d| d.object.name }).to eq(collection.map(&:name).join(sep))
      end
    end

    context "ブロックを渡さない場合" do
      subject do
        decorator.enumerate
      end

      it "は、各モデルデコレータのバナー要素を列挙すること" do
        is_expected.to have_tag(".record_banner", with: {"data-model" => "Person"}, count: 5)
        is_expected.to have_tag(".enumeration.separator", count: 4)
      end
    end

    context "第1引数にコレクションの総数より小さい数値を渡した場合" do
      subject do
        decorator.enumerate(3)
      end

      context "コレクションが`ActiveRecord::Relation`の場合" do
        it "は、渡した数だけ列挙し、一覧へのリンク付き総数要素をつけて返すこと" do
          is_expected.to have_tag(".record_banner", with: {"data-model" => "Person"}, count: 3)
          is_expected.to have_tag(".enumeration.separator", count: 2)
          is_expected.to have_tag(".enumeration.info", count: 1){
            with_tag("a", with: {href: "/people"}, text: "...(3/5)")
          }
        end
      end

      context "コレクションが配列の場合" do
        let(:collection){ create_list(:person, 5) }

        it "は、渡した数だけ列挙し、リンクなし総数要素をつけて返すこと" do
          is_expected.to have_tag(".record_banner", with: {"data-model" => "Person"}, count: 3)
          is_expected.to have_tag(".enumeration.separator", count: 2)
          is_expected.to have_tag(".enumeration.info", text: "...(3/5)", count: 1){
            without_tag("a")
          }
        end
      end
    end
  end
end
