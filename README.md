# DraperExt
Short description and motivation.

## Usage
How to use my plugin.

### 「属性」付きScaffold
```bash
$ rails generate scaffold person name:string sex:boolean height:decimal
```
### 「関連」付きScaffold
```bash
$ rails generate scaffold person parent:belongs_to children:has_many husband:has_one friends:has_and_belongs_to_many
```
### 既存のScaffoldに「属性」を追加
```bash
$ rails generate attributes person name:string sex:boolean height:decimal
```
### 既存のScaffoldに「関連」を追加
```bash
$ rails generate associations person parent:belongs_to children:has_many husband:has_one friends:has_and_belongs_to_many
```
### デコレータのみを生成
```bash
$ rails generate decorator person
```

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'draper_ext'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install draper_ext
```

ジェネレータ
```bash
$ rails generate draper:install
```
`/app/decorators/application_decorator.rb`を生成。
無くても機能する。

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
