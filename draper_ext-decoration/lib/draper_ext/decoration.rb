require 'draper_ext/decoration/definer'

module DraperExt
  module Decoration
    class << self

      # @param [Array<String>] keys 名前の各部分
      # @return [String] 連結したクラス名
      def join(*keys)
        keys.map{|key| key.to_s.camelize }.join
      end

      # @param [Array<String>] const_name_keys 定数名
      # @yield [const_name] 渡した名前の定数が見つからない場合に実行
      # @yieldparam [String] const_name 渡した文字列群を連結した定数名
      # @yieldreturn [Object] 定数が見つからない場合に返すオブジェクト
      # @return [Object] 定数値
      def const_fetch(*const_name_keys, &default_proc)
        default_proc ||= proc{ Decoration.base_decorator_class }
        const_name = join(*const_name_keys)
        begin
          const_name.constantize
        rescue NameError => err
          if const_name.split("::").include?(err.name.to_s)
            default_proc.call(const_name)
          else
            raise err
          end
        end
      end

      def decoratable(*args)
        definer = Definer.new(*args)
        yield(definer)
        definer.mod
      end

      def base_decorator_class
        require 'application_decorator'
        ApplicationDecorator
      rescue LoadError
        Draper::Decorator
      end
    end
  end
end
