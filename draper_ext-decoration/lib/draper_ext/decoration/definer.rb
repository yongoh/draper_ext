module DraperExt
  module Decoration
    class Definer

      # @!attribute [r] mod
      #   @return [Module] メソッドを定義するモジュール
      # @!attribute [rw] decorator_class_method_name
      #   @return [Symbol] デコレータクラス取得メソッド名
      # @!attribute [rw] decorate_method_name
      #   @return [Symbol] デコレートメソッド名
      # @!attribute [rw] markup_method_name
      #   @return [Symbol] マークアップメソッド名
      # @!attribute [rw] shortcut_definer_method_name
      #   @return [Symbol] ショートカットメソッド生成メソッド名
      attr_reader :mod
      attr_accessor :decorator_class_method_name, :decorate_method_name, :markup_method_name, :shortcut_definer_method_name

      # @param [String,Symbol] name 各メソッド名の語幹部分
      # @param [Module] mod メソッドを定義するモジュール
      # @param [Hash] options 作成する各メソッド名群
      def initialize(name, mod = Module.new, **options)
        @mod = mod
        @decorator_class_method_name = options.fetch(:decorator_class_method_name){ "#{name}_class".to_sym }
        @decorate_method_name = options.fetch(:decorate_method_name){ name.to_sym }
        @markup_method_name = options.fetch(:markup_method_name){ "#{name}_markup".to_sym }
        @shortcut_definer_method_name = options.fetch(:shortcut_definer_method_name){ name.to_sym }
      end

      # デコレータクラス取得メソッドを定義
      # @param [String,Symbol] method_name メソッド名
      # @yield [owner, *args] デコレータクラスを返す手続き
      # @yieldparam [Object] owner デコレーションメソッドを使ったレシーバ
      # @yieldparam [Array] args デコレーションメソッドに渡した引数群
      # @yieldreturn [Class<Draper::Decorator>] デコレータクラス
      def decorator_class_method(method_name = decorator_class_method_name, &decorator_class_proc)
        decorator_class_proc ||= proc{ Decoration.base_decorator_class }
        mod.class_eval do

          # @param [Array] args `decorator_class_proc`に渡す引数群
          # @yield [] クラス継承ブロック
          # @return [Class<Draper::Decorator>] デコレータクラス
          define_method(method_name) do |*args, &inherit_proc|
            klass = decorator_class_proc.call(self, *args)
            klass = Class.new(klass, &inherit_proc) if inherit_proc
            klass
          end
        end
      end

      # デコレートメソッドを定義
      # @param [String,Symbol] method_name メソッド名
      # @yield [klass, owner, *args] デコレータクラスをインスタンス化する手続き
      # @yieldparam [Class<Draper::Decorator>] klass デコレータクラス
      # @yieldparam [Object] owner デコレーションメソッドを使ったレシーバ
      # @yieldparam [Array] args デコレーションメソッドに渡した引数群
      # @yieldreturn [Draper::Decorator] デコレータインスタンス
      def decorate_method(method_name = decorate_method_name, &decorate_proc)
        decorate_proc ||= proc{|klass, *args, &block| klass.new(*args, &block) }
        this = self
        mod.class_eval do

          # @param [Array] args デコレータクラス取得メソッドと`decorate_proc`に渡す引数群
          # @param [Class,nil] decorator_class デコレータクラス。`nil`の場合はデコレータクラス取得メソッドを使用。
          # @yield [] クラス継承ブロック
          # @return [Draper::Decorator] レシーバをデコレートしたデコレータインスタンス
          define_method(method_name) do |*args, decorator_class: nil, **options, &block|
            decorator_class ||= send(this.decorator_class_method_name, *args, options, &block)
            decorate_proc.call(decorator_class, self, *args, options)
          end
        end
      end

      # マークアップメソッドを定義
      # @param [String,Symbol] method_name メソッド名
      # @yield [builder, decorator, *args] タグビルダーのデフォルト値を設定するブロック
      # @yieldparam [Markup::HtmlBuilder] builder デコレータの情報を含むタグビルダー
      # @yieldparam [Draper::Decorator] decorator デコレータ
      # @yieldparam [Array] args `#xxx_markup`に渡した引数群
      def markup_method(method_name = markup_method_name)
        this = self
        mod.class_eval do

          # @param [Draper::Decorator,String,Symbol] decorator デコレータ、もしくはデコレートメソッドへ渡す第1引数
          # @param [Array] args デコレートメソッドへ渡す引数群
          # @yield [] デコレートメソッドへ渡すブロック
          # @return [Markup::HtmlBuilder] デコレータの情報を含むタグビルダー
          define_method(method_name) do |decorator, *args, &block|
            decorator = send(this.decorate_method_name, decorator, *args, &block) if decorator.is_a?(String) || decorator.is_a?(Symbol)
            receivers = [decorator]
            receivers.concat(base_receivers) if respond_to?(:base_receivers, true)
            builder = Markup::HtmlBuilder.new(Markup::Agent.new(*receivers))
            yield(builder, decorator, *args) if block_given?
            builder
          end
        end
      end

      # ショートカットメソッドを作成するためのメソッドを持つモジュールを作成
      # @param [String,Symbol] singular_definer_name メソッド名
      # @return [Module] メソッドを持つモジュール
      def shortcut_definer_module(singular_definer_name = shortcut_definer_method_name)
        plural_definer_name = singular_definer_name.to_s.pluralize
        this = self
        Module.new do

          private

          # ショートカットメソッドを定義
          # @param [Array] args デコレートメソッドへ渡す引数群
          # @param [String] format メソッド名のフォーマット文字列
          define_method(singular_definer_name) do |*args, format: "%s", **options, &block|
            shortcut_method_name = sprintf(format, args.first.to_s)
            variable_name = "@#{shortcut_method_name}"

            # デコレーションメソッドへ移譲
            # @return [Draper::Decorator] デコレータインスタンス
            define_method(shortcut_method_name) do
              instance_variable_get(variable_name) || instance_variable_set(variable_name, send(this.decorate_method_name, *args, options, &block))
            end
          end

          # 複数のショートカットメソッドを定義
          # @param [Array<String,Symbol>] shortcut_method_names 作成するショートカットメソッドの名前群
          # @param [Hash] options ショートカットメソッド作成メソッドに渡すオプション
          define_method(plural_definer_name) do |*shortcut_method_names, **options, &block|
            shortcut_method_names.each do |shortcut_method_name|
              send(singular_definer_name, shortcut_method_name, options, &block)
            end
          end
        end
      end
    end
  end
end
