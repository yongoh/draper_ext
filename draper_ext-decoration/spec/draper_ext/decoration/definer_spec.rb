require 'rails_helper'

describe DraperExt::Decoration::Definer do
  let(:definer){ described_class.new("build") }
  let(:owner){ double("Owner").extend(definer.mod) }

  let(:default_decorator_class){ Draper::Decorator }
  let(:custom_decorator_class){ Struct.new(:object, :arg1, :arg2) }
  let(:decorator){ double("Decorator") }

  describe "#decorator_class_method" do
    context "引数なしの場合" do
      before do
        definer.decorator_class_method
      end

      it "は、`@decorator_class_method_name`の名前のインスタンスメソッドを`@mod`に定義すること" do
        expect(definer.decorator_class_method_name).to eq(:build_class)
        expect(definer.mod.instance_methods(false)).to include(:build_class)
      end
    end

    context "文字列もしくはシンボルを渡した場合" do
      before do
        definer.decorator_class_method(:foobar)
      end

      it "は、渡した名前のインスタンスメソッドを`@mod`に定義すること" do
        expect(definer.decorator_class_method_name).to eq(:build_class)
        expect(definer.mod.instance_methods(false)).to include(:foobar)
        expect(definer.mod.instance_methods(false)).not_to include(:build_class)
      end
    end

    context "ブロックなしの場合" do
      before do
        definer.decorator_class_method
      end

      describe "@mod.build_class" do
        it "は、既定デコレータクラスを返すこと" do
          expect(owner.build_class).to equal(default_decorator_class)
        end
      end
    end

    context "ブロックありの場合" do
      before do
        definer.decorator_class_method{ custom_decorator_class }
      end

      describe "@mod.build_class" do
        it "は、`#decorator_class_method`に渡したブロックの引数にレシーバと引数群を渡すこと" do
          expect{|block|
            definer.decorator_class_method(&block)
            owner.build_class(:foo, "bar")
          }.to yield_with_args(owner, :foo, "bar")
        end

        it "は、`#decorator_class_method`に渡したブロックの結果を返すこと" do
          expect(owner.build_class).to equal(custom_decorator_class)
        end
      end
    end
  end

  describe "#decorate_method" do
    before do
      allow(owner).to receive(:build_class).and_return(custom_decorator_class)
      definer.decorate_method
    end

    it "は、`@decorate_method_name`の名前のインスタンスメソッドを`@mod`に定義すること" do
      expect(definer.decorate_method_name).to eq(:build)
      expect(definer.mod.instance_methods(false)).to include(:build)
    end

    describe "@mod.build" do
      it "は、レシーバをデコレートしたデコレータインスタンスを返すこと" do
        expect(owner.build).to be_instance_of(custom_decorator_class).and have_attributes(object: owner)
      end

      context "オプション`:decorator_class`にクラスを渡した場合" do
        let(:custom_decorator_class_2){ Struct.new(:object, :arg1, :arg2) }

        it "は、そのクラスのインスタンスを返すこと" do
          expect(owner.build(decorator_class: custom_decorator_class_2))
            .to be_instance_of(custom_decorator_class_2)
            .and have_attributes(object: owner)
        end
      end
    end

    context "ブロックありの場合" do
      before do
        definer.decorate_method{ decorator }
      end

      describe "@mod.build" do
        it "は、`#decorate_method`に渡したブロックの引数にデコレータクラスとレシーバと引数群を渡すこと" do
          expect{|block|
            definer.decorate_method(&block)
            owner.build(:foo, "bar")
          }.to yield_with_args(custom_decorator_class, owner, :foo, "bar", {})
        end

        it "は、`#decorate_method`に渡したブロックの結果を返すこと" do
          expect(owner.build).to equal(decorator)
        end
      end
    end
  end

  describe "#markup_method" do
    before do
      definer.markup_method
    end

    it "は、`@markup_method_name`の名前のインスタンスメソッドを`@mod`に定義すること" do
      expect(definer.markup_method_name).to eq(:build_markup)
      expect(definer.mod.instance_methods(false)).to include(:build_markup)
    end

    describe "@mod.build_markup" do
      context "第1引数にオブジェクトを渡した場合" do
        subject do
          owner.build_markup(decorator)
        end

        it "は、渡したオブジェクトをレシーバに含むHTMLビルダーを返すこと" do
          is_expected.to be_a(Markup::HtmlBuilder)
          expect(subject.agent.receivers).to include(equal(decorator))
        end
      end

      context "第1引数に文字列またはSymbolを渡した場合" do
        subject do
          owner.build_markup(:foo, "bar")
        end

        before do
          allow(owner).to receive(:build).and_return(decorator)
        end

        it "は、引数をデコレートメソッドに渡して得られたオブジェクトをレシーバに含むHTMLビルダーを返すこと" do
          is_expected.to be_a(Markup::HtmlBuilder)
          expect(subject.agent.receivers).to include(equal(decorator))
        end
      end
    end

    context "ブロックありの場合" do
      describe "@mod.build_markup" do
        it "は、`#markup_method`に渡したブロックの引数にHTMLビルダーとデコレータと`#build_markup`に渡した引数群を渡すこと" do
          expect{|block|
            definer.markup_method(&block)
            owner.build_markup(decorator, :foo, :bar)
          }.to yield_with_args(
            be_a(Markup::HtmlBuilder).and(have_attributes(agent: have_attributes(receivers: include(equal(decorator))))),
            equal(decorator),
            :foo,
            :bar,
          )
        end
      end
    end
  end

  describe "#shortcut_definer_module" do
    let(:owner){ owner_class.new }
    let(:owner_class){
      mod = definer.shortcut_definer_module
      Class.new do
        extend mod
        def build(*args); Time.now end
      end
    }

    it "は、`@shortcut_definer_method_name`の名前の特異メソッドを`@mod`をincludeしたクラスに定義すること" do
      expect(definer.shortcut_definer_method_name).to eq(:build)
      expect(owner_class.private_methods(false)).to include(:build, :builds)
    end

    describe "::build" do
      context "オプション無しの場合" do
        before do
          owner_class.send(:build, :foo)
        end

        it "は、渡した名前を持つショートカットメソッドを作成すること" do
          expect(owner.foo).to be_instance_of(Time).and equal(owner.foo)
        end
      end

      context "オプション`:format`にメソッド名のフォーマットを渡した場合" do
        before do
          owner_class.send(:build, :foo, format: "hoge_%s_piyo")
        end

        it "は、フォーマットされた名前のショートカットメソッドを作成すること" do
          expect(owner.hoge_foo_piyo).to be_instance_of(Time).and equal(owner.hoge_foo_piyo)
          expect(owner.public_methods(false)).not_to include(:foo)
        end
      end
    end

    describe "::builds" do
      before do
        owner_class.send(:builds, :foo, :bar, :baz)
      end

      it "は、渡した名前のショートカットメソッド群を作成すること" do
        expect(owner.public_methods(false)).to include(:foo, :bar, :baz)
      end
    end
  end
end
