require 'rails_helper'

describe DraperExt::Decoration do
  let(:default_decorator_class){ Draper::Decorator }
  let(:owner){ double("Owner").extend(mod) }

  describe "::const_fetch" do
    context "渡した文字列を連結した名前の定数が存在する場合" do
      it "は、その定数値を返すこと" do
        expect(described_class.const_fetch("PersonDecorator")).to equal(PersonDecorator)
      end
    end

    context "渡した文字列を連結した名前の定数が存在しない場合" do
      context "ブロックを渡さない場合" do
        it "は、デフォルト値を返すこと" do
          expect(described_class.const_fetch("undefined", "decorator")).to equal(Draper::Decorator)
        end
      end

      context "ブロックを渡した場合" do
        let(:default){ double("Class") }

        it "は、渡した文字列を連結した名前をブロック引数に渡すこと" do
          expect{|block| described_class.const_fetch("undefined", "decorator", &block) }.to yield_successive_args("UndefinedDecorator")
        end

        it "は、ブロックの戻り値を返すこと" do
          expect(described_class.const_fetch("undefined", "decorator"){ default }).to equal(default)
        end
      end
    end

    context "名前空間つきの定数名を渡した場合" do
      it "は、その定数値を返すこと" do
        expect(described_class.const_fetch("foo", "/", "Bar", "::", "BazDecorator")).to equal(Foo::Bar::BazDecorator)
      end
    end

    context "定数以外の名前エラーを発生させた場合" do
      before do
        allow_any_instance_of(String).to receive(:constantize){ undefined_variable }
      end

      it "は、名前エラーを発生させること" do
        expect{
          described_class.const_fetch("undefined", "decorator")
        }.to raise_error(NameError, /undefined local variable or method `undefined_variable'/)
      end
    end
  end
end
