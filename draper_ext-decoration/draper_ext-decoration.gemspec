$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "draper_ext/decoration/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "draper_ext-decoration"
  s.version     = DraperExt::Decoration::VERSION
  s.authors     = ["yongoh"]
  s.email       = ["a.yongoh@gmail.com"]
  s.homepage    = ""
  s.summary     = ""
  s.description = ""
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails"
  s.add_dependency "draper"
  s.add_dependency "markup"

  s.add_development_dependency "rspec"
  s.add_development_dependency "rspec-html-matchers"
end
