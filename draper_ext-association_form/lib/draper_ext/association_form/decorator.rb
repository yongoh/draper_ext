module DraperExt
  module AssociationForm
    class Decorator < Draper::Decorator

      # @!attribute [r] form_builder
      #   @return [ActionView::Helpers::FormBuilder] form_builder フォームビルダー
      attr_reader :form_builder
      alias :f :form_builder

      # @!attribute [r] association
      #   @return [ActiveRecord::Associations::Association] 「関連」オブジェクト
      alias_method :association, :object

      # @!attribute [r] reflection
      #   @return [ActiveRecord::Reflection::AssociationReflection] リフレクションオブジェクト
      delegate :reflection, to: :association

      # @!attribute [r] name
      #   @return [Symbol] 関連名
      delegate :name, to: :reflection

      def initialize(form_builder, association, **options)
        @form_builder = form_builder
        association = f.object.association(association) if association.is_a?(String) || association.is_a?(Symbol)
        super(association, options)
      end

      def model_name
        association.owner.model_name
      end

      # @return [Boolean] 関連先が存在するかどうか
      def exists?
        association.scope.exists?
      end

      # @return [String] 翻訳した関連名
      def human_association_name
        reflection.active_record.human_attribute_name(name)
      end

      # @return [ActiveSupport::SafeBuffer] 関連先のActionField
      # @see #decorate_markup_block
      def action_field(*args, **options, &block)
        dm_options = options.extract!(:markup, :decorator_class)
        f.action_field(name, *args, options) do |*block_args|
          decorate_markup_block(*block_args, dm_options, &block)
        end
      end

      %i(
        reference_field
        singular_reference_field
        collection_reference_field
      ).each do |method_name|

        # @return [ActiveSupport::SafeBuffer] 関連先の参照フィールド
        # @see #decorate_markup_block
        define_method(method_name) do |*args, **options, &block|
          block ||= :banner.to_proc
          dm_options = options.extract!(:markup, :decorator_class)
          f.public_send(method_name, name, *args, options) do |*block_args|
            decorate_markup_block(*block_args, dm_options, &block)
          end
        end
      end

      # @return [ActiveSupport::SafeBuffer] 関連先の参照フィールド用ラベル要素
      def reference_label(*args, &block)
        f.reference_label(name, *args, &block)
      end

      private

      def base_receivers
        [form_builder, Markup::HtmlBuilder, helpers]
      end

      # @param [ActiveRecord::Base,ActionView::Helpers::FormBuilder] object レコードかフォームビルダー
      # @param [Boolean] markup マークアップ記法を使うかどうか
      # @param [Class<Draper::Decorator>] decorator_class デコレータクラス
      # @overload decorate_markup_block(object, markup: true){}
      #   @yield [m] マークアップ記法で内容を作るブロック
      #   @yieldparam [Markup::DomContainer] m 第1引数のデコレータをレシーバに含むDOMコンテナ
      # @overload decorate_markup_block(object, markup: false){}
      #   @yield [decorator]] 通常記法で内容を作るブロック
      #   @yieldparam [ActiveRecord::Base,ActionView::Helpers::FormBuilder] 第1引数のデコレータ
      def decorate_markup_block(object, markup: true, decorator_class: nil, &block)
        decorator = decorator_class ? decorator_class.new(object) : object.decorate
        if markup
          Markup.markup(decorator, object, Markup::HtmlBuilder, helpers, &block)
        else
          yield(decorator)
        end
      end
    end
  end
end
