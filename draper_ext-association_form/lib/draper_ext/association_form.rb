require 'draper'
require 'draper_ext/decoration'

require 'draper_ext/association_form/decorator'

module DraperExt
  module AssociationForm
    include Decoration.decoratable(:association){|definer|

      # #association_class
      definer.decorator_class_method do |owner, name, *args|
        Decoration.const_fetch(owner.model_name.singular, name, :association_form_decorator){ Decorator }
      end

      # #association
      definer.decorate_method{|klass, owner, name, *args| klass.new(owner.form_builder, name, *args) }

      # #association_markup
      definer.markup_method do |builder, assc|
        builder.default_html_attributes.merge!(
          class: %w(activerecord association),
          data: {
            model: assc.model_name.name,
            association: assc.name,
          }
        )
      end

      # ::association, ::associations
      ClassMethods = definer.shortcut_definer_module
    }
  end
end
