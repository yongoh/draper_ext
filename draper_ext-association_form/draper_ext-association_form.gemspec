$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "draper_ext/association_form/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "draper_ext-association_form"
  spec.version     = DraperExt::AssociationForm::VERSION
  spec.authors     = ["yongoh"]
  spec.email       = ["a.yongoh@gmail.com"]
  spec.homepage    = ""
  spec.summary     = ""
  spec.description = ""
  spec.license     = "MIT"

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "action_field"
  spec.add_dependency "reference_field"
  spec.add_dependency "draper_ext-decoration"

  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "rspec-rails"
  spec.add_development_dependency "database_cleaner"
  spec.add_development_dependency "factory_bot_rails"
  spec.add_development_dependency "rspec-html-matchers"
end
