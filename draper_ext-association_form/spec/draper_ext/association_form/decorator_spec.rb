require 'rails_helper'

describe DraperExt::AssociationForm::Decorator do
  let(:decorator){ described_class.new(form_builder, :parent) }
  let(:form_builder){ h.form_with(model: person){|f| return f } }
  let(:person){ create(:person, :with_parent) }

  let(:object_name){ "person[parent_attributes]" }

  describe "#human_association_name" do
    subject do
      decorator.human_association_name
    end

    context "訳文がある関連名の場合" do
      it "は、その訳文を返すこと" do
        is_expected.to eq("親")
      end
    end

    context "訳文が無い関連名の場合" do
      let(:decorator){ described_class.new(form_builder, :grandchildren) }

      it "は、英文化した関連名を返すこと" do
        is_expected.to eq("Grandchildren")
      end
    end
  end

  describe "#action_field" do
    it "は、関連先のActionFieldを返すこと" do
      expect(decorator.action_field).to have_tag(":root"){
        with_tag(".action_field-container", with: {"data-object-name" => "person[parent_attributes]"}){
          with_tag(".action_field-member", with: {"data-object-name" => "person[parent_attributes]"}, count: 1){
            with_hidden_field("person[parent_attributes][id]", person.parent.id)
            with_hidden_field("person[parent_attributes][_destroy]", "false")
          }
        }
        with_tag(".action_field-menu"){
          with_tag(".event_button[data-template]", with: {"data-event" => "action_field:create"})
          with_tag(".event_button", with: {"data-event" => "action_field:remove"})
        }
      }
    end

    context "オプション`:markup`に`true`を渡した場合 (default)" do
      it "は、関連先のフォームデコレータとフォームをレシーバに含むDOMコンテナをブロック引数に渡すこと" do
        expect{|block| decorator.action_field(&block) }.to yield_successive_args(
          be_a(Markup::DomContainer).and(have_attributes(_agent: have_attributes(receivers: include(
            be_instance_of(PersonFormDecorator).and(have_attributes(object: equal(person.parent))),
            be_a(ActionField::MemberFormBuilder).and(have_attributes(object: equal(person.parent))),
          )))),
          be_a(Markup::DomContainer).and(have_attributes(_agent: have_attributes(receivers: include(
            be_instance_of(PersonFormDecorator).and(have_attributes(object: be_a_new(Person))),
            be_a(ActionField::MemberFormBuilder).and(have_attributes(object: be_a_new(Person))),
          )))),
        )
      end
    end

    context "オプション`:markup`に`false`を渡した場合" do
      it "は、関連先のフォームデコレータをブロック引数に渡すこと" do
        expect{|block| decorator.action_field(markup: false, &block) }.to yield_successive_args(
          be_instance_of(PersonFormDecorator).and(have_attributes(object: equal(person.parent))),
          be_instance_of(PersonFormDecorator).and(have_attributes(object: be_a_new(Person))),
        )
      end
    end

    context "オプション`:decorator_class`にデコレータクラスを渡した場合" do
      let(:custom_decorator_class){
        Struct.new(:form_builder, :options) do
          delegate :object, to: :form_builder
        end
      }

      it "は、渡したクラスのデコレータインスタンスをブロック引数に渡すこと" do
        expect{|block| decorator.action_field(markup: false, decorator_class: custom_decorator_class, &block) }.to yield_successive_args(
          be_instance_of(custom_decorator_class).and(have_attributes(object: equal(person.parent))),
          be_instance_of(custom_decorator_class).and(have_attributes(object: be_a_new(Person))),
        )
      end
    end
  end

  describe "#reference_field" do
    it "は、関連先の参照フィールドを返すこと" do
      expect(decorator.reference_field).to have_tag(".reference_field-field"){
        with_tag("input.foreign_type", with: {type: "hidden", value: "Person"})
        with_tag("input.foreign_key", with: {type: "hidden", name: "person[parent_id]", value: person.parent.id.to_s})
        with_tag(".event_button", with: {"data-event" => "reference_field:find"})
        with_tag(".banner_container"){
          with_tag(".record_banner", with: {"data-model" => "Person", "data-record-id" => person.parent.id}, text: person.parent.name)
        }
      }
    end

    context "オプション`:markup`に`true`を渡した場合 (default)" do
      it "は、関連先のデコレータとレコードをレシーバに含むDOMコンテナをブロック引数に渡すこと" do
        expect{|block| decorator.reference_field(&block) }.to yield_with_args(
          be_a(Markup::DomContainer).and(have_attributes(_agent: have_attributes(receivers: include(
            be_instance_of(PersonModelDecorator).and(have_attributes(object: equal(person.parent))),
            equal(person.parent),
          )))),
        )
      end
    end

    context "オプション`:markup`に`false`を渡した場合" do
      it "は、関連先のデコレータをブロック引数に渡すこと" do
        expect{|block| decorator.reference_field(markup: false, &block) }.to yield_with_args(
          be_instance_of(PersonModelDecorator).and(have_attributes(object: equal(person.parent))),
        )
      end
    end

    context "オプション`:decorator_class`にデコレータクラスを渡した場合" do
      let(:custom_decorator_class){ Struct.new(:object, :options) }

      it "は、渡したクラスのデコレータインスタンスをブロック引数に渡すこと" do
        expect{|block| decorator.reference_field(markup: false, decorator_class: custom_decorator_class, &block) }.to yield_with_args(
          be_instance_of(custom_decorator_class).and(have_attributes(object: equal(person.parent))),
        )
      end
    end
  end

  describe "#collection_reference_field" do
    let(:decorator){ described_class.new(form_builder, :friends) }
    let(:person){ create(:person, :with_friends) }

    it "は、参照フィールドを含むActionFieldを返すこと" do
      expect(decorator.collection_reference_field).to have_tag(":root"){
        with_tag(".action_field-container"){
          person.friends.each_with_index do |friend, i|
            with_tag(".action_field-member", with: {"data-object-name" => "person[friends_attributes][#{i}]"}, count: 1){
              with_tag(".reference_field-field"){
                without_hidden_field("person[friend_types][]")
                with_hidden_field("person[friend_ids][]", friend.id)
                with_tag(".record_banner", with: {"data-model" => "Person", "data-record-id" => friend.id})
              }
            }
          end

          without_tag("input[name$='[id]']")
          without_tag("input[name$='[_destroy]']")
        }
        with_tag(".action_field-menu"){
          with_tag(".event_button[data-template]", with: {"data-event" => "action_field:add"})
          with_tag(".event_button", with: {"data-event" => "action_field:clear"})
        }
      }
    end
  end

  describe "#reference_label" do
    it "は、「探」イベントボタンに紐付けられたラベル要素を返すこと" do
      expect(decorator.reference_label).to have_tag("label", with: {for: "person_parent-find"}, text: "Parent")
    end
  end
end
