require 'rails_helper'

describe DraperExt::AssociationForm do
  let(:owner){ double("Owner", form_builder: form_builder, model_name: person.model_name).extend(described_class) }
  let(:form_builder){ h.form_with(model: person){|f| return f } }
  let(:person){ create(:person, :with_parent) }

  describe "#association" do
    it "は、渡した名前の「関連」フォームデコレータオブジェクトを返すこと" do
      expect(owner.association(:wife)).to be_instance_of(PersonWifeAssociationFormDecorator)
        .and have_attributes(
          form_builder: equal(form_builder),
          name: :wife,
        )
    end
  end

  describe "#association_markup.div" do
    it "は、渡した名前の関連の情報を持つ要素を生成すること" do
      expect(owner.association_markup("parent").div).to have_tag(
        "div.activerecord.association:empty",
        with: {
          "data-model" => "Person",
          "data-association" => "parent",
        },
      )
    end
  end
end
