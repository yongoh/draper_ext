module DraperExt
  module Ransack
    module Sort

      def sorts
        @sorts ||= Sorts.new(form_builder)
      end
    end
  end
end
