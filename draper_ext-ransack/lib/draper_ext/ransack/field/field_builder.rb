module DraperExt
  module Ransack
    module Field
      class FieldBuilder < DraperExt::Field::FieldBuilder

        # @!attribute [r] predicate
        #   @return [Symbol] 検索条件の述語
        # @!attribute [r] associations
        #   @return [Array<Symbol>] 関連名の配列
        attr_reader :predicate, :associations

        def initialize(form_builder, associations, attribute, predicate, **options, &block)
          super(form_builder, attribute, options, &block)
          @associations = associations.map(&:to_sym)
          @predicate = predicate.to_sym
        end

        def model_name
          object.klass.model_name
        end

        def method_name
          [*associations, attribute, predicate].join("_")
        end
      end
    end
  end
end
