module DraperExt
  module Ransack
    module Association
      class FormDecorator < Draper::Decorator

        # @!attribute [r] form_builder
        #   @return [ActionView::Helpers::FormBuilder] フォームビルダー
        # @!attribute [r] names
        #   @return [Array<Symbol>] 関連名の配列
        attr_reader :form_builder, :names
        alias :f :form_builder

        def initialize(form_builder, *names, **options)
          super(form_builder.object, options)
          @form_builder = form_builder
          @names = names.map(&:to_sym)
        end

        def model_name
          object.klass.model_name
        end

        # @return [String] 翻訳した関連名
        def human_association_name
          object.klass.human_attribute_name(name)
        end

        # @return [String] 連結した関連名
        def name
          names.join("_")
        end

        private

        def base_receivers
          [Markup::HtmlBuilder, helpers]
        end
      end
    end
  end
end
