module DraperExt
  module Ransack
    class Railtie < ::Rails::Railtie

      initializer "Add field and association features" do
        SearchForm.class_eval do
          include Form
          include Field
          include Association
          include Sort
        end
        SearchForm::ClassMethods.class_eval do
          include Association::ClassMethods
        end
        Association::FormDecorator.class_eval do
          include Field
          include Association
        end
        Sort::FormDecorator.class_eval do
          include Form
        end
      end

      initializer "Add decoration feature" do
        ActiveSupport.on_load(:action_controller) do
          ::Ransack::Helpers::FormBuilder.send(:include, Decoration.decoratable(:decorator){|definer|

            # #decorator_class
            definer.decorator_class_method do |form_builder|
              Decoration.const_fetch(form_builder.object.klass.model_name.singular, :search_form_decorator){ Search::FormDecorator }
            end

            # #decorate
            definer.decorate_method(:decorate)
          })
        end
      end

      initializer "Add model_name" do
        ::Ransack::Nodes::Sort.send(:include, Module.new{
          delegate :model_name, to: :klass
        })
      end
    end
  end
end
