module DraperExt
  module Ransack
    module Sort
      class Sorts

        # @!attribute [r] form_builder
        #   @return [Ransack::Helpers::FormBuilder] 検索用フォームビルダー
        attr_reader :form_builder
        alias_method :f, :form_builder

        def initialize(form_builder)
          @form_builder = form_builder
        end

        def name
          :sorts
        end

        # @teturn [String] ソート条件フィールドセットのタイトル
        # FIXME: 現状「分類状態」になっているが、これでは何のことやらわからない
        #   「ソート条件」あたりがいいのではないかと思う
        #   訳文ファイルは gem ransack 側にあるので弄れない
        def human_association_name
          ::Ransack::Translate.word(:sort) + ::Ransack::Translate.word(:condition)
        end

        # @return [ActiveSupport::SafeBuffer] コンテナ要素とイベントボタン群
        def action_field(sort_attributes = [], **options, &member_proc)
          options.fetch(:templates){|key| options[key] = [template_record] }
          h.action_field("#{f.object_name}[sorts]", f.object.sorts, collection: true, **options) do |member_form|
            decorator = Sort::FormDecorator.new(member_form)
            if block_given?
              yield(decorator)
            else
              h.concat(decorator.name_field(sort_attributes))
              h.concat(decorator.dir_field)
              h.concat(member_form.remove_button)
            end
          end
        end

        # @return [Ransack::Nodes::Sort] ソート条件オブジェクト（テンプレート用）
        def template_record
          ::Ransack::Nodes::Sort.new(f.object.context).build(name: f.object.klass.attribute_names.first)
        end

        private

        def h
          @h ||= f.instance_variable_get(:@template)
        end
      end
    end
  end
end
