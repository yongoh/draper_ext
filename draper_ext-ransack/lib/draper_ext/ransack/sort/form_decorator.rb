module DraperExt
  module Ransack
    module Sort
      class FormDecorator < Draper::Decorator

        # @param [Array<String>] attribute_names 属性名の配列
        # @return [ActiveSupport::SafeBuffer] ソート属性選択フィールドブロック
        def name_field(attribute_names, html_attributes = {})
          field_markup(:name).div(html_attributes) do |m|
            m.human_collection_select(attribute_names){|key| object.klass.human_attribute_name(key) }
          end
        end

        # @return [ActiveSupport::SafeBuffer] 昇順/降順フィールドブロック
        def dir_field(html_attributes = {})
          field_markup(:dir).div(html_attributes) do |m|
            m.human_collection_select(%i(asc desc)){|key| object.translate(key) }
          end
        end
      end
    end
  end
end
