module DraperExt
  module Ransack
    module SearchForm
      extend ActiveSupport::Concern

      module ClassMethods
      end

      def model_name
        object.klass.model_name
      end
    end
  end
end
