module DraperExt
  module Ransack
    module Association
      include Decoration.decoratable(:association){|definer|

        # #association_class
        definer.decorator_class_method do |owner, name, *args|
          Decoration.const_fetch(owner.model_name.singular, name, :ransack_association_form_decorator){ FormDecorator }
        end

        # #association
        definer.decorate_method do |klass, owner, *args|
          klass.new(owner.form_builder, *(owner.try(:names) || []), *args)
        end

        # #association_markup
        definer.markup_method do |builder, assc|
          builder.default_html_attributes.merge!(
            class: %w(activerecord association),
            data: {
              model: assc.model_name.name,
              association: assc.name,
            }
          )
        end

        # ::association, ::associations
        ClassMethods = definer.shortcut_definer_module
      }
    end
  end
end
