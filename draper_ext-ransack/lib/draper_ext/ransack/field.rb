module DraperExt
  module Ransack
    module Field
      include Decoration.decoratable(:field){|definer|

        # #field_class
        definer.decorator_class_method do |owner, attribute, *args|
          Decoration.const_fetch(owner.model_name.singular, attribute, :field_builder){ FieldBuilder }
        end

        # #field
        definer.decorate_method do |klass, owner, *args|
          klass.new(owner.form_builder, owner.try(:names) || [], *args)
        end

        # #field_markup
        definer.markup_method do |builder, field|
          builder.default_html_attributes.merge!(
            class: %w(activerecord field),
            data: {
              model: field.model_name.name,
              association: field.associations.join("_"),
              attribute: field.attribute,
              predicate: field.predicate,
              field_name: field.field_name,
            }
          )
        end
      }
    end
  end
end
