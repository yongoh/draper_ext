$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "draper_ext/ransack/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "draper_ext-ransack"
  spec.version     = DraperExt::Ransack::VERSION
  spec.authors     = ["yongoh"]
  spec.email       = ["a.yongoh@gmail.com"]
  spec.homepage    = ""
  spec.summary     = ""
  spec.description = ""
  spec.license     = "MIT"

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "ransack"
  spec.add_dependency "action_field"
  spec.add_dependency "draper_ext-form"
  spec.add_dependency "draper_ext-field"

  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "rspec-rails"
  spec.add_development_dependency "database_cleaner"
  spec.add_development_dependency "factory_bot_rails"
  spec.add_development_dependency "rspec-html-matchers"
end
