require 'rails_helper'

describe DraperExt::Ransack::Association do
  let(:owner){ double("Owner", form_builder: form_builder, model_name: Person.model_name).extend(described_class) }
  let(:form_builder){ h.search_form_for(Person.ransack){|f| return f } }

  describe "#association" do
    it "は、渡した名前の「関連」フォームデコレータオブジェクトを返すこと" do
      expect(owner.association(:wife)).to be_instance_of(PersonWifeRansackAssociationFormDecorator)
        .and have_attributes(
          form_builder: equal(form_builder),
          names: [:wife],
        )
    end
  end

  describe "#association_markup.div" do
    it "は、渡した名前の関連の情報を持つ要素を生成すること" do
      expect(owner.association_markup("parent").div).to have_tag(
        "div.activerecord.association:empty",
        with: {
          "data-model" => "Person",
          "data-association" => "parent",
        },
      )
    end
  end
end
