require 'rails_helper'

describe DraperExt::Ransack::Field::FieldBuilder do
  let(:field_builder){ described_class.new(form_builder, [:parent, :children], :name, :cont) }
  let(:form_builder){ h.search_form_for(Person.ransack){|f| return f } }

  describe "#field_name" do
    it "は、このフィールドに対応するURLパラメータのキーを返すこと" do
      expect(field_builder.field_name).to eq("q[parent_children_name_cont]")
    end
  end

  describe "#helper" do
    it "は、入力欄要素を生成すること" do
      expect(field_builder.helper(:text_field)).to have_tag(
        "input#q_parent_children_name_cont:not([value])",
        with: {
          type: "text",
          name: "q[parent_children_name_cont]",
        },
      )
    end
  end

  describe "#human_collection_select" do
    subject do
      field_builder.human_collection_select(["Haruhi", "Yuki", "Mikuru"])
    end

    it "は、値の訳文を内容に持つ選択肢を含む<select>要素を生成すること" do
      is_expected.to have_tag("select#q_parent_children_name_cont", with: {name: "q[parent_children_name_cont]"}){
        with_option("涼宮ハルヒ", "Haruhi")
        with_option("長門有希", "Yuki")
        with_option("朝比奈みくる", "Mikuru")
      }
    end
  end

  describe "#label" do
    it "は、ラベル要素を生成すること" do
      expect(field_builder.label).to have_tag("label", with: {for: "q_parent_children_name_cont"}, text: "人物 氏名 は以下を含む")
    end
  end
end
