require 'rails_helper'

describe DraperExt::Ransack::Field do
  let(:owner){ double("Owner", form_builder: form_builder, model_name: Person.model_name).extend(described_class) }
  let(:form_builder){ h.search_form_for(Person.ransack){|f| return f } }

  describe "#field" do
    it "は、渡した名前の検索フィールドビルダーを返すこと" do
      expect(owner.field(:name, :cont)).to be_instance_of(DraperExt::Ransack::Field::FieldBuilder)
        .and have_attributes(
          object: be_a(Ransack::Search),
          attribute: :name,
          predicate: :cont,
          method_name: "name_cont",
          field_name: "q[name_cont]",
        )
    end
  end

  describe "#field_markup.div" do
    it "は、渡した属性の情報を持つ要素を生成すること" do
      expect(owner.field_markup("name", "cont").div).to have_tag(
        "div.activerecord.field:empty",
        with: {
          "data-model" => "Person",
          "data-attribute" => "name",
          "data-predicate" => "cont",
          "data-field-name" => "q[name_cont]",
        },
      )
    end
  end
end
