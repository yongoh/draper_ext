require 'rails_helper'

describe DraperExt::Ransack::Sort::FormDecorator do
  let(:decorator){ described_class.new(form_builder) }
  let(:form_builder){ h.fields_for(sort){|f| return f } }
  let(:sort){ Ransack::Nodes::Sort.new(Person.ransack.context).build(name: "height", dir: "asc") }

  describe "#name_field" do
    subject do
      decorator.name_field(%w(name height age))
    end

    it "は、ソート属性選択フィールドブロックを生成すること" do
      field_name = "person[name]"

      is_expected.to have_tag("div.activerecord.field", with: {
        "data-model" => "Person",
        "data-attribute" => "name",
        "data-field-name" => field_name,
      }){
        with_select(field_name){
          with_tag("option", count: 3)

          with_option("氏名", "name")
          with_option("身長", "height", with: {selected: "selected"})
          with_option("年齢", "age")
        }
      }
    end
  end

  describe "#dir_field" do
    it "は、昇順/降順フィールドブロックを生成すること" do
      field_name = "person[dir]"

      expect(decorator.dir_field).to have_tag("div.activerecord.field", with: {
        "data-model" => "Person",
        "data-attribute" => "dir",
        "data-field-name" => field_name,
      }){
        with_select(field_name){
          with_tag("option", count: 2)

          with_option("昇順", "asc", with: {selected: "selected"})
          with_option("降順", "desc")
        }
      }
    end
  end
end
