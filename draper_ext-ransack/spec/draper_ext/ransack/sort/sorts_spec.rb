require 'rails_helper'

describe DraperExt::Ransack::Sort::Sorts do
  let(:sorts){ described_class.new(form_builder) }
  let(:form_builder){ h.search_form_for(Person.ransack){|f| return f } }

  describe "#human_association_name" do
    it{ expect(sorts.human_association_name).to eq("分類状態") }
  end

  describe "#action_field" do
    subject do
      sorts.action_field(%w(name height age))
    end

    shared_examples_for "have menu" do
      it "は、ボタン群を生成すること" do
        is_expected.to have_tag(".action_field-menu"){
          with_tag(".event_button", with: {"data-event" => "action_field:add"})
          with_tag(".event_button", with: {"data-event" => "action_field:clear"})
        }
      end
    end

    context "`Ransack::Search#sorts`が空の場合" do
      it "は、空のコンテナを生成すること" do
        is_expected.to have_tag(".action_field-container:empty", with: {"data-object-name" => "q[sorts]"})
      end

      it_behaves_like "have menu"
    end

    context "`Ransack::Search#sorts`をセットした場合" do
      before do
        form_builder.object.sorts = [{name: "name", dir: "asc"}, {name: "height", dir: "desc"}]
      end

      it "は、ソート条件入力欄群を含むコンテナを生成すること" do
        is_expected.to have_tag(".action_field-container", with: {"data-object-name" => "q[sorts]"}){
          with_tag(".action_field-member#q_sorts_0-action_field-member", count: 1){
            with_select("q[sorts][0][name]"){
              with_option("氏名", "name", with: {selected: "selected"})
              with_option("身長", "height")
              with_option("年齢", "age")
            }
            with_select("q[sorts][0][dir]"){
              with_option("昇順", "asc", with: {selected: "selected"})
              with_option("降順", "desc")
            }
            with_tag(".event_button", with: {"data-event" => "action_field:remove", for: "q_sorts_0-action_field-member"})
          }
          with_tag(".action_field-member#q_sorts_1-action_field-member", count: 1){
            with_select("q[sorts][1][name]"){
              with_option("氏名", "name")
              with_option("身長", "height", with: {selected: "selected"})
              with_option("年齢", "age")
            }
            with_select("q[sorts][1][dir]"){
              with_option("昇順", "asc")
              with_option("降順", "desc", with: {selected: "selected"})
            }
            with_tag(".event_button", with: {"data-event" => "action_field:remove", for: "q_sorts_1-action_field-member"})
          }

          with_tag(".action_field-member", count: 2)
        }
      end

      it_behaves_like "have menu"
    end

    context "ブロックを渡した場合" do
      it "は、ソート条件フォームデコレータをブロック引数に渡すこと" do
        expect{|block| sorts.action_field(markup: false, &block) }.to yield_successive_args(
          be_instance_of(DraperExt::Ransack::Sort::FormDecorator).and(have_attributes(
            form_builder: be_a(ActionField::MemberFormBuilder).and(have_attributes(
              object: be_a(::Ransack::Nodes::Sort),
              object_name: "q[sorts][0]"),
            ),
          )),
        )
      end
    end
  end
end
