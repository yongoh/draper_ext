require 'rails_helper'

describe DraperExt::Ransack::Association::FormDecorator do
  let(:decorator){ described_class.new(form_builder, :parent, :wife) }
  let(:form_builder){ h.search_form_for(Person.ransack){|f| return f } }

  describe "#human_association_name" do
    subject do
      decorator.human_association_name
    end

    context "訳文がある関連名の場合" do
      let(:decorator){ described_class.new(form_builder, :parent) }

      it "は、その訳文を返すこと" do
        is_expected.to eq("親")
      end
    end

    context "訳文が無い関連名の場合" do
      let(:decorator){ described_class.new(form_builder, :grandchildren) }

      it "は、英文化した関連名を返すこと" do
        is_expected.to eq("Grandchildren")
      end
    end

    context "存在しない関連名の場合" do
      let(:decorator){ described_class.new(form_builder, :undefined) }

      it "は、英文化した関連名を返すこと" do
        is_expected.to eq("Undefined")
      end
    end
  end

  describe "#field" do
    it "は、関連先における渡した名前の属性・述語の検索フィールドビルダーを返すこと" do
      expect(decorator.field(:name, :cont)).to be_a(DraperExt::Ransack::Field::FieldBuilder)
        .and have_attributes(
          form_builder: equal(form_builder),
          associations: [:parent, :wife],
          attribute: :name,
          field_name: "q[parent_wife_name_cont]",
        )
    end
  end

  describe "#association" do
    it "は、関連先における渡した名前の検索関連デコレータを返すこと" do
      expect(decorator.association(:children)).to be_a(described_class)
        .and have_attributes(
          form_builder: equal(form_builder),
          names: [:parent, :wife, :children],
        )
    end
  end

  describe "#field_markup.div" do
    it "は、関連先の属性の情報を持つ要素を生成すること" do
      expect(decorator.field_markup(:name, :eq).div).to have_tag(
        "div.activerecord.field",
        with: {
          "data-model" => "Person",
          "data-association" => "parent_wife",
          "data-attribute" => "name",
          "data-predicate" => "eq",
          "data-field-name" => "q[parent_wife_name_eq]",
        },
      )
    end
  end

  describe "#association_markup.div" do
    it "は、関連先の関連先の情報を持つ要素を生成すること" do
      expect(decorator.association_markup(:children).div).to have_tag(
        "div.activerecord.association",
        with: {
          "data-model" => "Person",
          "data-association" => "parent_wife_children",
        },
      )
    end
  end
end
