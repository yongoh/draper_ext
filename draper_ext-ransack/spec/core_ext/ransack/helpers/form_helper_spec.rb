require 'rails_helper'

describe Ransack::Helpers::FormHelper do
  let(:ransack){ Person.ransack }

  describe "#search_form_for" do
    context "オプション`:decoration`に`false`を渡した場合" do
      it "は、ブロック引数に検索用フォームビルダーを渡すこと (default)" do
        expect{|block| h.search_form_for(ransack, &block) }.to yield_with_args(
          be_instance_of(Ransack::Helpers::FormBuilder).and have_attributes(object: equal(ransack))
        )
      end
    end

    context "オプション`:decoration`に`true`を渡した場合" do
      it "は、ブロック引数に検索用フォームデコレータを渡すこと" do
        expect{|block| h.search_form_for(ransack, decoration: true, &block) }.to yield_with_args(
          be_instance_of(PersonSearchFormDecorator).and have_attributes(object: equal(ransack))
        )
      end
    end

    subject do
      h.search_form_for(ransack) do
        h.concat h.content_tag(:div, "(´･ω･｀)", id: "shobon")
        h.concat h.content_tag(:span, "（＾ω＾）", class: "boon")
      end
    end

    it "は、検索フォーム要素を生成すること" do
      is_expected.to have_form("/people", "get"){
        with_tag("div#shobon", text: "(´･ω･｀)")
        with_tag("span.boon", text: "（＾ω＾）")
      }
    end
  end
end
