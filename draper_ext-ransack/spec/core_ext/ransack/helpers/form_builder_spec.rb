require 'rails_helper'

describe Ransack::Helpers::FormBuilder do
  let(:form_builder){ h.search_form_for(ransack){|f| return f } }
  let(:ransack){ Person.ransack }

  describe "#decorate" do
    it "は、自身をデコレートしたフォームデコレータを返すこと" do
      expect(form_builder.decorate).to be_instance_of(PersonSearchFormDecorator)
        .and have_attributes(object: equal(ransack))
    end
  end
end
