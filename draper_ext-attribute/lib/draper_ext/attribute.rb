require 'draper'
require 'draper_ext/decoration'

require 'draper_ext/attribute/engine'
require 'draper_ext/attribute/humanizable'
require 'draper_ext/attribute/decorator'

module DraperExt
  module Attribute
    include Decoration.decoratable(:attribute){|definer|

      # #attribute_class
      definer.decorator_class_method do |owner, name, *args|
        Decoration.const_fetch(owner.model_name.singular, name, :attribute_decorator){ Decorator }
      end

      # #attribute
      definer.decorate_method{|klass, owner, name, *args| klass.new(owner.object, name, *args) }

      # #attribute_markup
      definer.markup_method do |builder, attr|
        builder.default_html_attributes.merge!(
          class: %w(activerecord attribute),
          data: {
            model: attr.model_name.name,
            attribute: attr.name,
            value: attr.value,
          }
        )
        builder.default_content_proc = proc{ Markup.h.concat(attr.humanize) }
      end

      # ::attribute, ::attributes
      ClassMethods = definer.shortcut_definer_module
    }
  end
end
