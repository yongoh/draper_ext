module DraperExt
  module Attribute
    class Decorator < Draper::Decorator
      include Humanizable

      # @!attribute [r] name
      #   @return [Symbol] 属性名
      attr_reader :name
      delegate :model_name, to: :object

      def initialize(object, name, options = {})
        super(object, options)
        @name = name.to_sym
      end

      # @return [Object] 属性値
      def value
        object.public_send(name)
      end
    end
  end
end
