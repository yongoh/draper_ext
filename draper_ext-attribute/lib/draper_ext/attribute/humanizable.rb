module DraperExt
  module Attribute
    module Humanizable

      # 属性値の訳文スコープの基底
      BASE_SCOPE = %w(activerecord attribute_values).map(&:freeze).freeze

      class << self

        # @param [String,Symbol] model モデル名の訳文キー
        # @param [String,Symbol] attribute 属性名の訳文キー
        # @param [String,Symbol] value 属性値の訳文キー
        # @return [String] 翻訳した属性値
        # @raise [TypeError] 属性名をキーとした値がハッシュではない場合に発生
        def human_attribute_key(model, attribute, value)
          i18n_hash = translate(model, attribute)
          if i18n_hash.is_a?(Hash)
            value = value.to_sym if value.respond_to?(:to_sym)
            i18n_hash.fetch(value, value).to_s
          else
            raise TypeError, "translation result of attribute '#{attribute}' is not Hash"
          end
        end

        # @param [String,Symbol] model モデル名の訳文キー
        # @param [String,Symbol] attribute 属性名の訳文キー
        # @param [String,Symbol] value 属性値の訳文キー
        # @return [String] 翻訳・式展開した属性値
        def human_attribute_value(model, attribute, value)
          translate(model, attribute, model: model, attribute: attribute, value: value).to_s
        end

        # 訳文の種類によって処理を振り分ける
        # @param [String,Symbol] model モデル名の訳文キー
        # @param [String,Symbol] attribute 属性名の訳文キー
        # @param [String,Symbol] value 属性値の訳文キー
        # @return [String] 翻訳した属性値
        def humanize(model, attribute, value)
          translation = translate(model, attribute, default: Proc.new{ return value.to_s })
          case translation
          when Hash then human_attribute_key(model, attribute, value)
          when String then human_attribute_value(model, attribute, value)
          else translation.to_s
          end
        end

        def translate(*args, **options)
          I18n.translate((BASE_SCOPE + args).join("."), options)
        end
      end

      # @return [String] 翻訳した属性名
      def human_attribute_name
        object.class.human_attribute_name(name)
      end

      %i(human_attribute_key human_attribute_value humanize).each do |method_name|

        # @return [String] 翻訳した属性値
        define_method(method_name) do
          Humanizable.send(method_name, object.model_name.i18n_key, name, value)
        end
      end

      # @return [ActiveSupport::SafeBuffer] 翻訳した時間を内容に含む`time`要素
      # @note `#value`が時間オブジェクトを返す場合にのみ対応
      def time(*args, &block)
        h.content_tag(:time, h.localize(value, *args, &block), datetime: value.iso8601)
      end
    end
  end
end
