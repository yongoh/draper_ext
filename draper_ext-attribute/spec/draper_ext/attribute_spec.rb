require 'rails_helper'

describe DraperExt::Attribute do
  let(:owner){ double("Owner", object: person, model_name: person.model_name, ).extend(described_class) }
  let(:person){ create(:person, name: "伊達政宗", height: 159.4) }

  describe "#attribute" do
    it "は、渡した属性名の属性デコレータを返すこと" do
      expect(owner.attribute(:name)).to be_instance_of(DraperExt::Attribute::Decorator)
        .and have_attributes(
          object: equal(person),
          name: :name,
        )
    end
  end

  describe "#attribute_markup.div" do
    it "は、渡した名前の属性の情報を持つ要素を生成すること" do
      expect(owner.attribute_markup(:name).div).to have_tag(
        "div.activerecord.attribute",
        with: {
          "data-model" => "Person",
          "data-attribute" => "name",
          "data-value" => "伊達政宗",
        },
        text: "伊達政宗",
      )
    end
  end
end
