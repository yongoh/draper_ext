require 'rails_helper'

describe DraperExt::Attribute::Decorator do
  let(:decorator){ decorator_class.new(person, :name) }
  let(:decorator_class){ described_class }
  let(:person){ create(:person) }

  describe "#value" do
    it "は、属性値を返すこと" do
      expect(decorator.value).to eq(person.name)
    end
  end
end
