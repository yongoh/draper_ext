require 'rails_helper'

describe DraperExt::Attribute::Humanizable do
  let(:owner){ double(object: person, name: :name).extend(described_class) }
  let(:person){ create(:person) }

  describe "::human_attribute_key" do
    context "属性名の訳文ハッシュに属性値と同じキーが存在する場合" do
      subject do
        described_class.human_attribute_key("person", "name", "Mikuru")
      end

      it "は、翻訳した属性値を返すこと" do
        is_expected.to eq("朝比奈みくる")
      end
    end

    context "属性名の訳文ハッシュに属性値と同じキーが存在しない場合" do
      subject do
        described_class.human_attribute_key("person", "name", "Kyon")
      end

      it "は、文字列化した属性値を返すこと" do
        is_expected.to eq("Kyon")
      end
    end

    context "属性名の訳文がハッシュではない場合" do
      it "は、型エラーを発生させること" do
        expect{
          described_class.human_attribute_key("person", "hoge", "Hoge!")
        }.to raise_error(TypeError, "translation result of attribute 'hoge' is not Hash")
      end
    end

    context "属性名の訳文が無い場合" do
      it "は、型エラーを発生させること" do
        expect{
          described_class.human_attribute_key("person", "unknown", "Unknown!")
        }.to raise_error(TypeError, "translation result of attribute 'unknown' is not Hash")
      end
    end

    context "属性値が`nil`の場合" do
      subject do
        described_class.human_attribute_key("person", "name", nil)
      end

      it{ is_expected.to eq("") }
    end

    context "属性値が数値の場合" do
      subject do
        described_class.human_attribute_key("person", "name", 1)
      end

      it "は、数値のキーの訳文を返すこと" do
        is_expected.to eq("number key")
      end
    end

    context "属性値が数字の場合" do
      subject do
        described_class.human_attribute_key("person", "name", "1")
      end

      it "は、数字のキーの訳文を返すこと" do
        is_expected.to eq("string key")
      end
    end

    context "訳文キーが真偽値の場合" do
      context "属性値が真偽値`true`の場合" do
        subject do
          described_class.human_attribute_key("person", "name", true)
        end

        it "は、真偽値`true`のキーの訳文を返すこと" do
          is_expected.to eq("boolean true key")
        end
      end

      context "属性値が真偽値`false`の場合" do
        subject do
          described_class.human_attribute_key("person", "name", false)
        end

        it "は、真偽値`false`のキーの訳文を返すこと" do
          is_expected.to eq("boolean false key")
        end
      end

      context "属性値が文字列'true'の場合" do
        subject do
          described_class.human_attribute_key("person", "name", "true")
        end

        it "は、文字列'true'のキーの訳文を返すこと" do
          is_expected.to eq("string true key")
        end
      end

      context "属性値が文字列'false'の場合" do
        subject do
          described_class.human_attribute_key("person", "name", "false")
        end

        it "は、文字列'false'のキーの訳文を返すこと" do
          is_expected.to eq("string false key")
        end
      end
    end

    context "属性値の訳文が文字列以外の場合" do
      subject do
        described_class.human_attribute_key("person", "name", "one")
      end

      it "は、文字列化した訳文を返すこと" do
        is_expected.to eq("1")
      end
    end
  end

  describe "::human_attribute_value" do
    context "訳文が式展開文字列の場合" do
      subject do
        described_class.human_attribute_value("person", "shobon", "しょぼ～ん")
      end

      it "は、属性値をパラメータとして式展開した訳文を返すこと" do
        is_expected.to eq("shobon: (´･ω･｀) <しょぼ～ん")
      end
    end

    context "訳文が文字列以外の場合" do
      subject do
        described_class.human_attribute_value("person", "number", "two")
      end

      it "は、文字列化した訳文を返すこと" do
        is_expected.to eq("123")
      end
    end

    context "訳文が存在しない場合" do
      subject do
        described_class.human_attribute_value("person", "one", 1)
      end

      it "は、訳文が見つからなかったことを示す文字列を返すこと" do
        is_expected.to eq("translation missing: ja.activerecord.attribute_values.person.one")
      end
    end
  end

  describe "::humanize" do
    context "属性名を訳文キーとした値がハッシュの場合" do
      subject do
        described_class.humanize("person", "name", "Yuki")
      end

      it "は、`::human_attribute_key`に移譲した結果を返すこと" do
        is_expected.to eq("長門有希")
      end
    end

    context "属性名を訳文キーとした値が文字列の場合" do
      subject do
        described_class.humanize("person", "shobon", "しょぼ～ん")
      end

      it "は、`::human_attribute_value`に移譲した結果を返すこと" do
        is_expected.to eq("shobon: (´･ω･｀) <しょぼ～ん")
      end
    end

    context "属性名を訳文キーとした値がそれ以外の場合" do
      subject do
        described_class.humanize("person", "number", "two")
      end

      it "は、文字列化した訳文を返すこと" do
        is_expected.to eq("123")
      end
    end

    context "属性名を訳文キーとした訳文が存在しない場合" do
      subject do
        described_class.humanize("person", "one", 1)
      end

      it "は、文字列化した属性値を返すこと" do
        is_expected.to eq("1")
      end
    end
  end

  describe "#human_attribute_name" do
    it "は、翻訳された属性名を返すこと" do
      expect(owner.human_attribute_name).to eq("氏名")
    end
  end

  describe "#human_attribute_key" do
    before do
      allow(owner).to receive(:value).and_return("Mikuru")
    end

    it "は、翻訳した属性値を返すこと" do
      expect(owner.human_attribute_key).to eq("朝比奈みくる")
    end
  end

  describe "#human_attribute_value" do
    before do
      allow(owner).to receive(:name).and_return(:shobon)
      allow(owner).to receive(:value).and_return("しょぼぼん")
    end

    it "は、属性値をパラメータとして式展開した訳文を返すこと" do
      expect(owner.human_attribute_value).to eq("shobon: (´･ω･｀) <しょぼぼん")
    end
  end

  describe "#humanize" do
    context "属性名を訳文キーとした値がハッシュの場合" do
      before do
        allow(owner).to receive(:value).and_return("Haruhi")
      end

      it "は、`::human_attribute_key`に移譲した結果を返すこと" do
        expect(owner.humanize).to eq("涼宮ハルヒ")
      end
    end

    context "属性名を訳文キーとした値が文字列の場合" do
      before do
        allow(owner).to receive(:name).and_return(:shobon)
        allow(owner).to receive(:value).and_return("しょぼぼん")
      end

      it "は、`::human_attribute_value`に移譲した結果を返すこと" do
        expect(owner.humanize).to eq("shobon: (´･ω･｀) <しょぼぼん")
      end
    end
  end

  describe "#time" do
    context "`#value`が`Time`オブジェクトを返す場合" do
      let(:owner){ double(h: helpers, value: time).extend(described_class) }
      let(:time){ Time.now }

      it "は、翻訳した時間を内容に持つ`time`要素を返すこと" do
        expect(owner.time).to have_tag("time", with: {datetime: time.iso8601}, text: h.l(time))
      end

      context "引数を渡した場合" do
        it "は、時間翻訳メソッドに引数を委譲すること" do
          expect(owner.time(formats: :long)).to have_tag("time", with: {datetime: time.iso8601}, text: h.l(time, formats: :long))
        end
      end
    end

    context "`#value`が`Time`オブジェクト以外を返す場合" do
      let(:owner){ double(h: helpers, value: "Mikuru").extend(described_class) }
      it{ expect{ owner.time }.to raise_error(I18n::ArgumentError) }
    end
  end
end
