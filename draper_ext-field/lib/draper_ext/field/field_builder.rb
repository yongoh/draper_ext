module DraperExt
  module Field
    class FieldBuilder < Draper::Decorator

      # @!attribute [r] form_builder
      #   @return [ActionView::Helpers::FormBuilder] フォームビルダー
      # @!attribute [r] attribute
      #   @return [Symbol] 属性名
      attr_reader :form_builder, :attribute
      alias :f :form_builder
      alias_method :method_name, :attribute
      delegate :model_name, to: :object

      def initialize(form_builder, attribute, options = {})
        super(form_builder.object, options)
        @form_builder = form_builder
        @attribute = attribute.to_sym
      end

      # @return [String] このフィールドに対応するURLパラメータのキー
      def field_name
        sprintf("%s[%s]", f.object_name, method_name)
      end

      # @param [String,Symbol] helper_method_name フォームヘルパー名
      # @param [Array] args フォームヘルパーに渡す引数群
      # @return [ActiveSupport::SafeBuffer] 入力欄要素
      def helper(helper_method_name, *args, &block)
        f.public_send(helper_method_name, method_name, *args, &block)
      end

      # フォームヘルパーメソッド群
      HELPERS = ActionView::Helpers::FormBuilder.public_instance_methods(false) - public_instance_methods
      HELPERS.each do |helper_method_name|
        define_method(helper_method_name) do |*args, &block|
          helper(helper_method_name, *args, &block)
        end
      end

      # <option> or <optgroup>用選択肢クラス
      # @!attribute [rw] key
      #   @return [String] 選択肢の値
      # @!attribute [rw] humanize_proc
      #   @return [Proc] 翻訳処理関数
      # @!attribute [rw] child_keys
      #   @return [Array] このグループに含まれる選択肢の値
      Item = Struct.new(:key, :humanize_proc, :child_keys) do

        # @return [String] 翻訳済みの選択肢の値
        def human
          humanize_proc.call(key)
        end

        # @return [Array<Item>] このグループに含まれる選択肢オブジェクトの配列
        def children
          child_keys.map{|child_key| self.class.new(child_key, humanize_proc) }
        end
      end

      # @param [Array<String>] keys 選択肢の値の配列
      # @param [Hash] options `ActionView::Helpers::FormBuilder#collection_select`へ渡すオプション
      # @param [Hash] html_attributes <select>のHTML属性
      # @yield [key] 値の翻訳処理
      # @yieldparam [String] key 各選択肢の値
      # @yieldreturn [String] 各選択肢の翻訳済みの値
      # @return [ActiveSupport::SafeBuffer] `keys`に対応した<option>を含む<select>
      def human_collection_select(keys, options = {}, html_attributes = {}, &humanize_proc)
        humanize_proc ||= method(:translate)
        items = keys.map{|key| Item.new(key, humanize_proc) }
        helper(:collection_select, items, :key, :human, options, html_attributes)
      end

      # @param [Array,Hash] keys グループ化した選択肢の値の配列
      # @param [Hash] options `ActionView::Helpers::FormBuilder#grouped_collection_select`へ渡すオプション
      # @param [Hash] html_attributes <select>のHTML属性
      # @yield [key] 値の翻訳処理
      # @yieldparam [String] key 各選択肢の値
      # @yieldreturn [String] 各選択肢の翻訳済みの値
      # @return [ActiveSupport::SafeBuffer] `keys`に対応した<optgroup>を含む<select>
      def human_grouped_collection_select(keys, options = {}, html_attributes = {}, &humanize_proc)
        humanize_proc ||= method(:translate)
        items = keys.map{|group_key, child_keys| Item.new(group_key, humanize_proc, child_keys) }
        helper(:grouped_collection_select, items, :children, :human, :key, :human, options, html_attributes)
      end

      # @param [String] key この属性の値
      # @return [String] 値の訳文
      def translate(key)
        I18n.t(key, scope: i18n_scope)
      end

      private

      # 属性値の訳文スコープの基底
      BASE_SCOPE = %w(activerecord attribute_values).map(&:freeze).freeze

      # @return [Array<String>] 属性値の訳文スコープ
      def i18n_scope
        (BASE_SCOPE + [model_name.i18n_key, attribute])
      end
    end
  end
end
