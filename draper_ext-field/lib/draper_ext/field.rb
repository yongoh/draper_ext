require 'draper_ext/field/field_builder'

module DraperExt
  module Field
    include Decoration.decoratable(:field){|definer|

      # #field_class
      definer.decorator_class_method do |owner, name, *args|
        Decoration.const_fetch(owner.model_name.singular, name, :field_builder){ FieldBuilder }
      end

      # #field
      definer.decorate_method{|klass, owner, name, *args| klass.new(owner.form_builder, name, *args) }

      # #field_markup
      definer.markup_method do |builder, field|
        builder.default_html_attributes.merge!(
          class: %w(activerecord field),
          data: {
            model: field.model_name.name,
            attribute: field.attribute,
            field_name: field.field_name,
          }
        )
      end

      # ::field, ::fields
      ClassMethods = definer.shortcut_definer_module
    }
  end
end
