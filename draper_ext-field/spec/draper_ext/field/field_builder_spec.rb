require 'rails_helper'

describe DraperExt::Field::FieldBuilder do
  let(:field_builder){ described_class.new(form_builder, :name) }
  let(:form_builder){ h.form_with(model: person){|f| return f } }
  let(:person){ create(:person) }

  describe "#field_name" do
    it "は、このフィールドに対応するURLパラメータのキーを返すこと" do
      expect(field_builder.field_name).to eq("person[name]")
    end
  end

  describe "#helper" do
    context "第1引数にフォームヘルパー名を渡した場合" do
      subject do
        field_builder.helper(:text_field)
      end

      it "は、入力欄要素を生成すること" do
        is_expected.to have_tag("*"){
          with_text_field("person[name]", person.name)
        }
      end
    end

    context "第2引数にHTML属性を渡した場合" do
      subject do
        field_builder.helper(:text_field, value: "てきすと")
      end

      it "は、渡したHTML属性を持つ入力欄要素を生成すること" do
        is_expected.to have_tag("*"){
          with_text_field("person[name]", "てきすと")
        }
      end
    end

    context "オプション`:name`に`nil`を渡した場合" do
      subject do
        field_builder.helper(:text_field, name: nil)
      end

      it "は、name属性を持たない入力欄要素を生成すること" do
        is_expected.to have_tag("input:not([name])", with: {type: "text", value: person.name})
      end
    end
  end

  describe "ヘルパーメソッド" do
    it "は、入力欄要素を生成すること" do
      expect(field_builder.text_field).to have_tag("*"){
        with_text_field("person[name]", person.name)
      }
    end
  end

  describe "#translate" do
    context "訳文が用意されている値を渡した場合" do
      it "は、訳文を返すこと" do
        expect(field_builder.translate("Haruhi")).to eq("涼宮ハルヒ")
      end
    end

    context "訳文が用意されていない値を渡した場合" do
      it "は、訳文が見つからなかったことを示す文章を返すこと" do
        expect(field_builder.translate("Unknown")).to eq("translation missing: ja.activerecord.attribute_values.person.name.Unknown")
      end
    end
  end

  describe "#human_collection_select" do
    let(:person){ create(:person, name: "Yuki") }

    context "ブロックを渡さない場合" do
      subject do
        field_builder.human_collection_select(["Haruhi", "Yuki", "Mikuru"])
      end

      it "は、`I18n.t`で翻訳した訳文を持つ選択肢を含む<select>要素を生成すること" do
        is_expected.to have_tag("*"){
          with_select("person[name]"){
            with_option("涼宮ハルヒ", "Haruhi")
            with_option("長門有希", "Yuki", with: {selected: "selected"})
            with_option("朝比奈みくる", "Mikuru")
          }
        }
      end
    end

    context "ブロックを渡した場合" do
      subject do
        field_builder.human_collection_select(["Haruhi", "Yuki", "Mikuru"]){|key| names[key] }
      end

      let(:names){
        {
          "Haruhi" => "すずみやはるひ",
          "Yuki" => "ながもん",
          "Mikuru" => "朝比奈ミクル",
        }
      }

      it "は、ブロック内で翻訳した訳文を持つ選択肢を含む<select>要素を生成すること" do
        is_expected.to have_tag("*"){
          with_select("person[name]"){
            with_option("すずみやはるひ", "Haruhi")
            with_option("ながもん", "Yuki", with: {selected: "selected"})
            with_option("朝比奈ミクル", "Mikuru")
          }
        }
      end
    end

    context "第2引数にオプションを渡した場合" do
      subject do
        field_builder.human_collection_select(["Haruhi", "Yuki", "Mikuru"], selected: "Mikuru")
      end

      it "は、オプションを`ActionView::Helpers::FormBuilder#collection_select`に渡すこと" do
        is_expected.to have_tag("*"){
          with_select("person[name]"){
            with_option("涼宮ハルヒ", "Haruhi")
            with_option("長門有希", "Yuki")
            with_option("朝比奈みくる", "Mikuru", with: {selected: "selected"})
          }
        }
      end
    end

    context "第3引数にHTML属性を渡した場合" do
      subject do
        field_builder.human_collection_select(["Haruhi", "Yuki", "Mikuru"], {}, id: "my-id", class: "my-class")
      end

      it "は、渡したHTML属性を持つ<select>要素を生成すること" do
        is_expected.to have_tag("select#my-id.my-class", with: {name: "person[name]"}){
          with_option("涼宮ハルヒ", "Haruhi")
          with_option("長門有希", "Yuki", with: {selected: "selected"})
          with_option("朝比奈みくる", "Mikuru")
        }
      end
    end
  end

  describe "#human_grouped_collection_select" do
    let(:person){ create(:person, name: "Yuki") }

    subject do
      field_builder.human_grouped_collection_select({
        "sos-dan" => %w(Haruhi Yuki Mikuru),
        "hostile force" => %w(Fujiwara Tachibana Suou),
      })
    end

    it "は、グループ化した選択肢を持つ<select>要素を生成すること" do
      is_expected.to have_tag("*"){
        with_select("person[name]"){
          with_tag("optgroup", with: {label: "SOS団"}){
            with_option("涼宮ハルヒ", "Haruhi")
            with_option("長門有希", "Yuki", with: {selected: "selected"})
            with_option("朝比奈みくる", "Mikuru")
          }
          with_tag("optgroup", with: {label: "敵対勢力"}){
            with_option("藤原", "Fujiwara")
            with_option("橘京子", "Tachibana")
            with_option("周防九曜", "Suou")
          }
        }
      }
    end
  end
end
