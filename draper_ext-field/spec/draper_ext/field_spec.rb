require 'rails_helper'

describe DraperExt::Field do
  let(:owner){ double("Owner", form_builder: form_builder, model_name: person.model_name).extend(described_class) }
  let(:form_builder){ h.form_with(model: person){|f| return f } }
  let(:person){ create(:person, name: "伊達政宗", height: 159.4) }

  describe "#field" do
    it "は、渡した属性名のフィールドビルダーを返すこと" do
      expect(owner.field(:name)).to be_instance_of(DraperExt::Field::FieldBuilder)
        .and have_attributes(
          form_builder: equal(form_builder),
          object: equal(person),
          attribute: :name,
        )
    end
  end

  describe "#field_markup.div" do
    it "は、渡した名前の属性の情報を持つ要素を生成すること" do
      expect(owner.field_markup("height").div).to have_tag(
        "div.activerecord.field:empty",
        with: {
          "data-model" => "Person",
          "data-attribute" => "height",
          "data-field-name" => "person[height]",
        },
      )
    end
  end
end
