require 'generators/rspec'
require 'rails/generators/resource_helpers'
require 'draper_ext/generators/hook_for_attributes_and_associations'

module Rspec
  module Generators
    class DecoratorGenerator < Base
      include Rails::Generators::ResourceHelpers
      include DraperExt::Generators::HookForAttributesAndAssociations

      source_root File.expand_path("../templates", __FILE__)

      def create_root_folder
        empty_directory root_folder
      end

      %w(model table article form search_form).each do |context|
        define_method("create_#{context}_decorator_spec_file") do
          template "#{context}_decorator_spec.rb.erb", File.join(root_folder, "#{singular_name}_#{context}_decorator_spec.rb")
        end
      end

      hook_for_arguments :attributes, :associations

      protected

      def root_folder
        File.join('spec', 'decorators', controller_file_path)
      end
    end
  end
end
