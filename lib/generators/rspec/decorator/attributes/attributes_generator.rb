require 'generators/rspec'
require 'rails/generators/resource_helpers'
require 'draper_ext/generators/attributes'

module Rspec
  module Generators
    module Decorator
      class AttributesGenerator < Base
        include Rails::Generators::ResourceHelpers
        include DraperExt::Generators::Attributes

        source_root File.expand_path("../templates", __FILE__)

        %w(model table article form search_form).each do |context|
          define_method("insert_into_#{context}_decorator_spec_file") do
            insert_into_file(
              File.join(root_folder, "#{singular_name}_#{context}_decorator_spec.rb"),
              indent(stemplate("#{context}_decorator_spec.rb.erb"), 2),
              before: /^end/,
            )
          end
        end

        protected

        def root_folder
          File.join('spec', 'decorators', controller_file_path)
        end
      end
    end
  end
end
