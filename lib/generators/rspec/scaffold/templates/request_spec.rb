require 'rails_helper'

describe "<%= class_name.pluralize %>", <%= type_metatag(:request) %> do
  let(:<%= singular_name %>){ create(:<%= singular_name %>) }

  let(:valid_attributes){ attributes_for(:<%= singular_name %>) }
  let(:invalid_attributes){
    attributes = attributes_for(:<%= singular_name %>, :invalid)
    skip("Add invalid attributes for the :invalid trait in your factory") if attributes.empty?
    attributes
  }

  let(:json){ JSON.parse(response.body) }

  shared_examples_for "create" do
    it "creates a new <%= class_name %>" do
      expect{ subject.call }.to change{ <%= class_name %>.count }.by(1)
    end
  end

  shared_examples_for "don't create" do
    it "don't creates a new <%= class_name %>" do
      expect{ subject.call }.not_to change{ <%= class_name %>.count }
    end
  end

  shared_examples_for "update" do
    it "updates the requested <%= ns_file_name %>" do
      skip("Add valid attributes for your factory") if valid_attributes.empty?
      expect{ subject.call }.to change{ <%= singular_name %>.reload.attributes }
        .from(include(<%= singular_name %>.attributes.slice(*valid_attributes.keys.map(&:to_s))))
        .to(include(valid_attributes.stringify_keys))
    end
  end

  shared_examples_for "don't update" do
    it "don't updates the requested <%= ns_file_name %>" do
      expect{ subject.call }.not_to change{ <%= singular_name %>.reload.attributes }
    end
  end

  shared_examples_for "destroy" do
    it "destroys the requested <%= ns_file_name %>" do
      expect{ <%= singular_name %>.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  shared_examples_for "json error messages" do
    it "returns a JSON containing error messages" do
      subject.call
      expect(json.keys).to contain_exactly(*invalid_attributes.keys.map(&:to_s))
    end
  end

  describe "GET <%= route_url %>" do
    before do
      create_list(:<%= singular_name %>, 20)
      get <%= index_helper %>_url
    end

    it "returns a '200 OK' status response" do
      expect(response).to have_http_status(200).and have_attributes(media_type: "text/html")
    end

    it "renders a list of <%= plural_name %>" do
      expect(response.body).to have_tag("table.index"){
        <%= class_name %>.all.each do |<%= singular_name %>|
          with_tag("tr", with: {"data-model" => "<%= class_name %>", "data-record-id" => <%= singular_name %>.id})
        end
      }
    end
  end

  describe "GET <%= route_url %>.json" do
    before do
      create_list(:<%= singular_name %>, 20)
      get <%= index_helper %>_url(format: :json)
    end

    it "returns a '200 OK' status response" do
      expect(response).to have_http_status(200).and have_attributes(media_type: "application/json")
    end

    it "renders a list of <%= plural_name %>" do
      expect(json["<%= plural_name %>"]).to match(<%= class_name %>.all.map{|<%= singular_name %>| include("id" => <%= singular_name %>.id) })
    end
  end

  describe "GET <%= route_url %>/1" do
    subject do
      proc{ get <%= show_helper %> }
    end

    context "the requested <%= ns_file_name %> exists" do
      before do
        subject.call
      end

      it "returns a '200 OK' status response" do
        expect(response).to have_http_status(200).and have_attributes(media_type: "text/html")
      end

      it "renders an article of the requested <%= ns_file_name %>" do
        expect(response.body).to have_tag("article.show", with: {"data-model" => "<%= class_name %>", "data-record-id" => <%= singular_name %>.id})
      end
    end

    context "the requested <%= ns_file_name %> don't exist" do
      before{ <%= singular_name %>.destroy }
      it{ expect{ subject.call }.to raise_error(ActiveRecord::RecordNotFound) }
    end
  end

  describe "GET <%= route_url %>/1.json" do
    subject do
      proc{ get <%= show_helper("format: :json") %> }
    end

    context "the requested <%= ns_file_name %> exists" do
      before do
        subject.call
      end

      it "returns a '200 OK' status response" do
        expect(response).to have_http_status(200).and have_attributes(media_type: "application/json")
      end

      it "returns a JSON containing the requested <%= ns_file_name %> details" do
        expect(json).to include("id" => <%= singular_name %>.id)
      end
    end

    context "the requested <%= ns_file_name %> don't exist" do
      before{ <%= singular_name %>.destroy }
      it{ expect{ subject.call }.to raise_error(ActiveRecord::RecordNotFound) }
    end
  end

  describe "GET <%= route_url %>/new" do
    before do
      get <%= new_helper %>
    end

    it "returns a '200 OK' status response" do
      expect(response).to have_http_status(200).and have_attributes(media_type: "text/html")
    end

    it "renders a form to create a new <%= class_name %>" do
      expect(response.body).to have_form("<%= route_url %>", "post")
    end
  end

  describe "GET <%= route_url %>/1/edit" do
    before do
      get <%= edit_helper %>
    end

    it "returns a '200 OK' status response" do
      expect(response).to have_http_status(200).and have_attributes(media_type: "text/html")
    end

    it "renders a form to update the requested <%= ns_file_name %>" do
      expect(response.body).to have_form("<%= route_url %>/#{<%= singular_name %>.id}", "post")
    end
  end

  describe "POST <%= route_url %>" do
    subject do
      proc{ post <%= index_helper %>_url(<%= ns_file_name %>: <%= singular_name %>_params) }
    end

    context "with valid params" do
      let(:<%= singular_name %>_params){ valid_attributes }

      it_behaves_like "create"

      it "returns a '302 Found' status response" do
        subject.call
        expect(response).to have_http_status(302).and have_attributes(media_type: "text/html")
      end

      it "redirects to the created <%= ns_file_name %>" do
        subject.call
        expect(response).to redirect_to(<%= class_name %>.last)
      end
    end

    context "with invalid params" do
      let(:<%= singular_name %>_params){ invalid_attributes }

      it_behaves_like "don't create"

      it "returns a '200 OK' status response" do
        subject.call
        expect(response).to have_http_status(200).and have_attributes(media_type: "text/html")
      end

      it "renders a list of error explanation" do
        subject.call
        expect(response.body).to have_tag(".error_explanation")
      end
    end
  end

  describe "POST <%= route_url %>.json" do
    subject do
      proc{ post <%= index_helper %>_url(format: :json, <%= ns_file_name %>: <%= singular_name %>_params) }
    end

    context "with valid params" do
      let(:<%= singular_name %>_params){ valid_attributes }

      it_behaves_like "create"

      it "returns a '201 Created' status response" do
        subject.call
        expect(response).to have_http_status(201).and have_attributes(media_type: "application/json")
      end

      it "returns a JSON containing the created <%= ns_file_name %> details" do
        subject.call
        expect(json).to include("id" => <%= class_name %>.last.id)
      end
    end

    context "with invalid params" do
      let(:<%= singular_name %>_params){ invalid_attributes }

      it_behaves_like "don't create"

      it "returns a '422 Unprocessable Entity' status response" do
        subject.call
        expect(response).to have_http_status(422).and have_attributes(media_type: "application/json")
      end

      it_behaves_like "json error messages"
    end
  end

  describe "PUT <%= route_url %>/1" do
    subject do
      proc{ put <%= show_helper("#{ns_file_name}: #{singular_name}_params") %> }
    end

    context "with valid params" do
      let(:<%= singular_name %>_params){ valid_attributes }

      it_behaves_like "update"

      it "returns a '302 Found' status response" do
        subject.call
        expect(response).to have_http_status(302).and have_attributes(media_type: "text/html")
      end

      it "redirects to the requested <%= ns_file_name %>" do
        subject.call
        expect(response).to redirect_to(<%= singular_name %>)
      end
    end

    context "with invalid params" do
      let(:<%= singular_name %>_params){ invalid_attributes }

      it_behaves_like "don't update"

      it "returns a '200 OK' status response" do
        subject.call
        expect(response).to have_http_status(200).and have_attributes(media_type: "text/html")
      end

      it "renders a list of error explanation" do
        subject.call
        expect(response.body).to have_tag(".error_explanation")
      end
    end
  end

  describe "PUT <%= route_url %>/1.json" do
    subject do
      proc{ put <%= show_helper("format: :json", "#{ns_file_name}: #{singular_name}_params") %> }
    end

    context "with valid params" do
      let(:<%= singular_name %>_params){ valid_attributes }

      it_behaves_like "update"

      it "returns a '200 OK' status response" do
        subject.call
        expect(response).to have_http_status(200).and have_attributes(media_type: "application/json")
      end

      it "returns a JSON containing the requested <%= ns_file_name %> details" do
        subject.call
        expect(json).to include("id" => <%= singular_name %>.id)
      end
    end

    context "with invalid params" do
      let(:<%= singular_name %>_params){ invalid_attributes }

      it_behaves_like "don't update"

      it "returns a '422 Unprocessable Entity' status response" do
        subject.call
        expect(response).to have_http_status(422).and have_attributes(media_type: "application/json")
      end

      it_behaves_like "json error messages"
    end
  end

  describe "DELETE <%= route_url %>/1" do
    before do
      delete <%= show_helper %>
    end

    it_behaves_like "destroy"

    it "returns a '302 Found' status response" do
      expect(response).to have_http_status(302).and have_attributes(media_type: "text/html")
    end

    it "redirects to the <%= plural_name %> list" do
      expect(response).to redirect_to(<%= index_helper %>_url)
    end
  end

  describe "DELETE <%= route_url %>/1.json" do
    before do
      delete <%= show_helper("format: :json") %>
    end

    it_behaves_like "destroy"

    it "returns a '204 No Content' status response" do
      expect(response).to have_http_status(204).and have_attributes(media_type: be_nil, body: be_blank)
    end
  end

  describe "GET <%= route_url %>/search" do
    before do
      get <%= search_helper %>
    end

    it "returns a '200 OK' status response" do
      expect(response).to have_http_status(200).and have_attributes(media_type: "text/html")
    end

    it "renders a form to search for <%= plural_name %>" do
      expect(response.body).to have_form("<%= route_url %>", "get")
    end
  end
end
