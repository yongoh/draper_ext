require 'rails_helper'

<%- module_namespacing do -%>
describe <%= controller_class_name %>Controller, <%= type_metatag(:controller) %> do
  let(:<%= ns_file_name %>){ create(:<%= ns_file_name %>) }

  let(:valid_attributes){ attributes_for(:<%= ns_file_name %>) }
  let(:invalid_attributes){
    attributes = attributes_for(:<%= ns_file_name %>, :invalid)
    skip("Add invalid attributes for the :invalid trait in your factory") if attributes.empty?
    attributes
  }
  let(:valid_session){ {} }

  <%- unless options[:singleton] -%>
  describe "GET #index" do
    before do
      create_list(:<%= ns_file_name %>, 10)
    end

    it "returns a success response" do
      get :index, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end

  <%- end -%>
  describe "GET #show" do
    it "returns a success response" do
      get :show, params: {id: <%= ns_file_name %>.to_param}, session: valid_session
      expect(response).to be_successful
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      get :edit, params: {id: <%= ns_file_name %>.to_param}, session: valid_session
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      subject do
        proc{ post :create, params: {<%= ns_file_name %>: valid_attributes}, session: valid_session }
      end

      it "creates a new <%= class_name %>" do
        expect{ subject.call }.to change{ <%= class_name %>.count }.by(1)
      end

      it "redirects to the created <%= ns_file_name %>" do
        subject.call
        expect(response).to redirect_to(<%= class_name %>.last)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {<%= ns_file_name %>: invalid_attributes}, session: valid_session
        expect(response).to be_successful
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      subject do
        proc{ put :update, params: {id: <%= ns_file_name %>.to_param, <%= ns_file_name %>: valid_attributes}, session: valid_session }
      end

      it "updates the requested <%= ns_file_name %>" do
        skip("Add valid attributes for your factory") if valid_attributes.empty?
        expect{ subject.call }.to change{ <%= ns_file_name %>.reload.attributes }
          .from(include(<%= ns_file_name %>.attributes.slice(*valid_attributes.keys.map(&:to_s))))
          .to(include(valid_attributes.stringify_keys))
      end

      it "redirects to the <%= ns_file_name %>" do
        subject.call
        expect(response).to redirect_to(<%= ns_file_name %>)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        put :update, params: {id: <%= ns_file_name %>.to_param, <%= ns_file_name %>: invalid_attributes}, session: valid_session
        expect(response).to be_successful
      end
    end
  end

  describe "DELETE #destroy" do
    subject do
      proc{ delete :destroy, params: {id: <%= ns_file_name %>.to_param}, session: valid_session }
    end

    before do
      <%= ns_file_name %>
    end

    it "destroys the requested <%= ns_file_name %>" do
      expect{ subject.call }.to change{ <%= class_name %>.count }.by(-1)
    end

    it "redirects to the <%= plural_name %> list" do
      subject.call
      expect(response).to redirect_to(<%= index_helper %>_url)
    end
  end

  <%- unless options[:singleton] -%>
  describe "GET #search" do
    it "returns a success response" do
      get :search, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end
  <%- end -%>
end
<%- end -%>
