require 'generators/rspec/scaffold/scaffold_generator'

Rspec::Generators::ScaffoldGenerator.class_eval do
  source_paths << File.expand_path('../templates', __FILE__)
  remove_command :generate_view_specs

  def create_root_folder
    empty_directory "spec/views/#{controller_name}"
  end

  protected

  def show_helper(*argstrs)
    super().sub(/\(@(\w+)\)/, "(#{['\1', *argstrs].join(", ")})")
  end

  def search_helper
    "search_#{index_helper}_url"
  end
end
