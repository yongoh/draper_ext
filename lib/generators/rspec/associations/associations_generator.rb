require 'generators/rspec'

module Rspec
  module Generators
    class AssociationsGenerator < Base

      class_option :orm

      hook_for :fixture_replacement do |invokator, invokatee|
        invokator.invoke invokatee if invokator.options["orm"] && !invokator.options["skip"]
      end

      hook_for :decorator, default: "decorator:associations"
      hook_for :template_engine, as: "scaffold:rspec:associations"
      hook_for :jbuilder, default: "jbuilder:associations"
    end
  end
end
