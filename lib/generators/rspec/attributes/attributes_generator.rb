require 'generators/rspec'

module Rspec
  module Generators
    class AttributesGenerator < Base

      class_option :orm

      hook_for :fixture_replacement do |invokator, invokatee|
        invokator.invoke invokatee if invokator.options["orm"] && !invokator.options["skip"]
      end

      hook_for :decorator, default: "decorator:attributes"
      hook_for :template_engine, as: "scaffold:rspec:attributes"
      hook_for :jbuilder, default: "jbuilder:attributes"
    end
  end
end
