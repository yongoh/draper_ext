require 'generators/rspec/reference_field/ajax/ajax_generator'

Rspec::Generators::ReferenceField::AjaxGenerator.class_eval do

  source_root File.expand_path("../templates", __FILE__)

  def add_testcase
    inject_into_file "spec/requests/#{controller_name}_spec.rb", before: %r{\n.+"GET /#{controller_name}/new"} do
      <<~EOF

      describe "GET /#{controller_name}.js" do
        it "raise security exception" do
          expect{
            get #{controller_name}_url(format: :js, ajax_target: "#ajax_target")
          }.to raise_error(ActionController::InvalidCrossOriginRequest, /\\ASecurity warning:/)
        end
      end
      EOF
      .gsub(/^/, " " * 2)
    end
  end
end
