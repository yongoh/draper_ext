require 'generators/rspec'
require 'rails/generators/resource_helpers'
require 'draper_ext/generators/associations'

module Rspec
  module Generators
    module Jbuilder
      class AssociationsGenerator < Base
        include Rails::Generators::ResourceHelpers
        include DraperExt::Generators::Associations

        source_root File.expand_path("../templates", __FILE__)

        def insert_into_index_view_spec_files
          insert_into_file(
            File.join(root_folder, "index.json.jbuilder_spec.rb"),
            indent(stemplate("matchers.rb.erb"), 10),
            before: /^\s*"url" =>/,
          )
        end

        def insert_into_show_view_spec_files
          insert_into_file(
            File.join(root_folder, "show.json.jbuilder_spec.rb"),
            indent(stemplate("matchers.rb.erb"), 6),
            before: /^\s*"url" =>/,
          )
        end

        protected

        def root_folder
          File.join('spec', 'views', controller_file_path)
        end
      end
    end
  end
end
