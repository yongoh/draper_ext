require 'generators/rspec'
require 'rails/generators/resource_helpers'
require 'draper_ext/generators/hook_for_attributes_and_associations'

module Rspec
  module Generators
    class JbuilderGenerator < Base
      include Rails::Generators::ResourceHelpers
      include DraperExt::Generators::HookForAttributesAndAssociations

      source_root File.expand_path("../templates", __FILE__)

      def copy_spec_files
        available_views.each do |view|
          template "#{view}.json.jbuilder_spec.rb.erb", File.join(root_folder, "#{view}.json.jbuilder_spec.rb")
        end
      end

      hook_for_arguments :attributes, :associations

      protected

      def root_folder
        File.join('spec', 'views', controller_file_path)
      end

      def available_views
        %w(index show)
      end
    end
  end
end
