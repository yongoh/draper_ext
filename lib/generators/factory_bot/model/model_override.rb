require 'generators/factory_bot/model/model_generator'
require 'draper_ext/generators/hook_for_attributes_and_associations'

FactoryBot::Generators::ModelGenerator.class_eval do
  include DraperExt::Generators::HookForAttributesAndAssociations

  hook_for_arguments :attributes, :associations, in: base_name

  private

  def filepath
    File.join(options[:dir], "#{filename}.rb")
  end

  def factory_definition
    source = File.expand_path("templates/factory_definition.rb.erb", __dir__)
    text = ERB.new(File.binread(source), nil, "-").result(binding)
    indent(text, 2)
  end
end
