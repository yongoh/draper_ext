require 'draper_ext/generators/attributes'

module FactoryBot
  module Generators
    class AttributesGenerator < Rails::Generators::NamedBase
      include DraperExt::Generators::Attributes

      source_root File.expand_path("../templates", __FILE__)

      def insert_default_attributes
        insert_into_file(
          factory_file_path,
          indent(stemplate("defaults.rb.erb"), 4),
          after: /factory :#{singular_table_name}.*\n/,
        )
      end

      def insert_invalid_attributes
        insert_into_file(
          factory_file_path,
          indent(stemplate("invalids.rb.erb"), 6),
          after: /trait :invalid do\n/,
        )
      end

      private

      def factory_file_path
        File.join("spec", "factories", plural_name + ".rb")
      end
    end
  end
end
