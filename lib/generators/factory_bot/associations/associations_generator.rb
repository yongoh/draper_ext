require 'draper_ext/generators/associations'

module FactoryBot
  module Generators
    class AssociationsGenerator < Rails::Generators::NamedBase
      include DraperExt::Generators::Associations

      source_root File.expand_path("../templates", __FILE__)

      def insert_association_traits
        insert_into_file(
          factory_file_path,
          indent(stemplate("traits.rb.erb"), 4),
          before: /^  end/,
        )
      end

      private

      def factory_file_path
        File.join("spec", "factories", plural_name + ".rb")
      end
    end
  end
end
