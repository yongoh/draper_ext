require 'rails/generators/named_base'
require 'rails/generators/resource_helpers'
require 'draper_ext/generators/attributes'

module Erb
  module Generators
    module Scaffold
      class AttributesGenerator < Rails::Generators::NamedBase
        include Rails::Generators::ResourceHelpers
        include DraperExt::Generators::Attributes

        source_root File.expand_path("../templates", __FILE__)

        def insert_into_index_view_files
          filepath = File.join(root_folder, "index.html.erb")
          # TODO: 挿入箇所指定をより厳密に
          insert_into_file(
            filepath,
            indent(stemplate("th.html.erb"), 6),
            before: /^\s*<th colspan="\d+" class="actions">.*<\/th>/,
          )
          insert_into_file(
            filepath,
            indent(stemplate("td.html.erb"), 6),
            before: /^\s*<%= #{singular_name}.action_link_cells %>/,
          )
          increase_colspan(filepath)
        end

        def insert_into_show_view_files
          insert_into_file(
            File.join(root_folder, "show.html.erb"),
            indent(stemplate("section.html.erb"), 2),
            before: /^ *<\/article>/,
          )
        end

        def insert_into_form_view_files
          insert_into_file(
            File.join(root_folder, "_form.html.erb"),
            indent(stemplate("field.html.erb"), 2),
            before: /^ *<div class="form-actions">/,
          )
        end

        def insert_into_search_view_files
          insert_into_file(
            File.join(root_folder, "search.html.erb"),
            indent(stemplate("field.html.erb"), 2),
            before: /^ *<%= f.sorts_field_set %>/,
          )
        end

        protected

        def root_folder
          File.join('app', 'views', controller_file_path)
        end

        # 見出しセルの`colspan`属性の値を属性の数だけ増加
        # NOTE: `rails destroy erb:scaffold:attributes`には対応していない
        def increase_colspan(filepath)
          gsub_file(filepath, /<th\b.+<%= #{class_name}.model_name.human %><\/th>/) do |matched|
            matched.sub(/\bcolspan="(\d+)"/){ %(colspan="#{$1.to_i + attributes.size}") }
          end
        end
      end
    end
  end
end
