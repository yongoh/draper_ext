require 'rails/generators/erb/scaffold/scaffold_generator'
require 'draper_ext/generators/hook_for_attributes_and_associations'

Erb::Generators::ScaffoldGenerator.class_eval do
  include DraperExt::Generators::HookForAttributesAndAssociations

  source_root File.expand_path("../templates", __FILE__)

  hook_for_arguments :attributes, :associations
  hook_for :test_framework, in: "erb:scaffold"

  def insert_into_controller_navigation_view_files
    insert_into_file(
      "app/views/layouts/_controller_navigation.html.erb",
      %(\n    <%= render "#{controller_file_path}/controller_navigation" %>),
      before: %r(\n\s*</ul>\s*</nav>\s*\z)
    )
  end

  private

  def available_views
    %w(
      _controller_navigation
      _action_navigation
      index
      edit
      show
      new
      _form
      search
    )
  end
end
