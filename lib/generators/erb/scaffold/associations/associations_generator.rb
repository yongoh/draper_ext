require 'rails/generators/named_base'
require 'rails/generators/resource_helpers'
require 'draper_ext/generators/associations'

module Erb
  module Generators
    module Scaffold
      class AssociationsGenerator < Rails::Generators::NamedBase
        include Rails::Generators::ResourceHelpers
        include DraperExt::Generators::Associations

        source_root File.expand_path("../templates", __FILE__)

        def insert_into_index_view_files
          filepath = File.join(root_folder, "index.html.erb")
          # TODO: 追加する順番が他と違う
          insert_into_file(
            filepath,
            indent(stemplate("th.html.erb"), 6),
            after: /<%= #{class_name}.model_name.human %>.*\n/,
          )
          insert_into_file(
            filepath,
            indent(stemplate("td.html.erb"), 6),
            after: /<%= #{singular_name}.action_link_cells %>.*\n/,
          )
        end

        def insert_into_show_view_files
          insert_into_file(
            File.join(root_folder, "show.html.erb"),
            indent(stemplate("section.html.erb"), 2),
            before: /^ *<\/article>/,
          )
        end

        def insert_into_form_view_files
          insert_into_file(
            File.join(root_folder, "_form.html.erb"),
            indent(stemplate("field_set.html.erb"), 2),
            before: /^ *<div class="form-actions">/,
          )
        end

        def insert_into_search_view_files
          insert_into_file(
            File.join(root_folder, "search.html.erb"),
            indent(stemplate("field_set.html.erb"), 2),
            before: /^ *<%= f.sorts_field_set %>/,
          )
        end

        protected

        def root_folder
          File.join('app', 'views', controller_file_path)
        end
      end
    end
  end
end
