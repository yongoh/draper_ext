require 'generators/rspec'
require 'rails/generators/resource_helpers'
require 'draper_ext/generators/hook_for_attributes_and_associations'

module Erb
  module Generators
    module Scaffold
      class RspecGenerator < Rspec::Generators::Base
        include Rails::Generators::ResourceHelpers
        include DraperExt::Generators::HookForAttributesAndAssociations

        source_root File.expand_path("../templates", __FILE__)

        def copy_spec_files
          available_views.each do |view|
            template "#{view}.html.erb_spec.rb.erb", File.join(root_folder, "#{view}.html.erb_spec.rb")
          end
        end

        hook_for_arguments :attributes, :associations, in: namespace

        protected

        def root_folder
          File.join('spec', 'views', controller_file_path)
        end

        def available_views
          %w(
            _controller_navigation
            _action_navigation
            index
            edit
            show
            new
            search
          )
        end
      end
    end
  end
end
