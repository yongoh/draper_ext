require 'generators/rspec'
require 'rails/generators/resource_helpers'
require 'draper_ext/generators/attributes'

module Erb
  module Generators
    module Scaffold
      module Rspec
        class AttributesGenerator < ::Rspec::Generators::Base
          include Rails::Generators::ResourceHelpers
          include DraperExt::Generators::Attributes

          source_root File.expand_path("../templates", __FILE__)

          # TODO: 挿入箇所指定をより厳密に
          def insert_into_index_view_spec_files
            filepath = File.join(root_folder, "index.html.erb_spec.rb")
            insert_into_file(
              filepath,
              indent(stemplate("th_matchers.rb.erb"), 10),
              before: /^\s*with_tag\("th.actions", with: \{colspan: 3\}\)/,
            )
            insert_into_file(
              filepath,
              indent(stemplate("td_matchers.rb.erb"), 12),
              before: /^\s*with_tag\("td\.action\.show\.link"\)/,
            )
            increase_colspan(filepath)
          end

          # HACK: 挿入箇所が最後のテストケースであることを前提にしている
          def insert_into_show_view_spec_files
            insert_into_file(
              File.join(root_folder, "show.html.erb_spec.rb"),
              indent(stemplate("section_matchers.rb.erb"), 6),
              before: /^\s*}\s*end\s*end\s*\z/,
            )
          end

          def insert_into_edit_view_spec_files
            insert_into_file(
              File.join(root_folder, "edit.html.erb_spec.rb"),
              indent(stemplate("field_matchers_with_value.rb.erb"), 6),
              before: /^\s*with_tag\(".form-actions"\)/,
            )
          end

          def insert_into_new_view_spec_files
            insert_into_file(
              File.join(root_folder, "new.html.erb_spec.rb"),
              indent(stemplate("field_matchers_without_value.rb.erb"), 6),
              before: /^\s*with_tag\(".form-actions"\)/,
            )
          end

          def insert_into_search_view_spec_files
            insert_into_file(
              File.join(root_folder, "search.html.erb_spec.rb"),
              indent(stemplate("search_field_matchers.rb.erb"), 6),
              before: /^\s*with_tag\("fieldset#sorts"\)/,
            )
          end

          protected

          def root_folder
            File.join('spec', 'views', controller_file_path)
          end

          # 見出しセルの`colspan`属性の検証の値を属性の数だけ増加
          # NOTE: `rails destroy erb:scaffold:rspec:attributes`には対応していない
          def increase_colspan(filepath)
            gsub_file(filepath, /with_tag\("th.model".+$/) do |matched|
              matched.sub(/(\bcolspan:\s*)(\d+)/){ $1 + ($2.to_i + attributes.size).to_s }
            end
          end
        end
      end
    end
  end
end
