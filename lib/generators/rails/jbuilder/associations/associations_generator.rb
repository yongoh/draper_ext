require 'rails/generators/named_base'
require 'rails/generators/resource_helpers'
require 'draper_ext/generators/associations'

module Rails
  module Generators
    module Jbuilder
      class AssociationsGenerator < NamedBase
        include ResourceHelpers
        include DraperExt::Generators::Associations

        source_root File.expand_path("../templates", __FILE__)

        def insert_into_partial_view_files
          insert_into_file(
            File.join(root_folder, "_#{singular_table_name}.json.jbuilder"),
            stemplate("partial.json.jbuilder.erb"),
            before: /^\s*json.url\b/,
          )
        end

        protected

        def root_folder
          File.join('app', 'views', controller_file_path)
        end
      end
    end
  end
end
