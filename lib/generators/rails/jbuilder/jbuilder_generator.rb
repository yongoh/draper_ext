require 'rails/generators/named_base'
require 'rails/generators/resource_helpers'
require 'draper_ext/generators/hook_for_attributes_and_associations'

module Rails
  module Generators
    class JbuilderGenerator < NamedBase
      include ResourceHelpers
      include DraperExt::Generators::HookForAttributesAndAssociations

      source_root File.expand_path("../templates", __FILE__)

      def copy_view_files
        available_views.each do |view|
          template "#{view}.json.jbuilder.erb", File.join(root_folder, "#{view}.json.jbuilder")
        end
      end

      def copy_partial_view_file
        template 'partial.json.jbuilder.erb', File.join(root_folder, "_#{singular_table_name}.json.jbuilder")
      end

      hook_for_arguments :attributes, :associations
      hook_for :test_framework

      protected

      def root_folder
        File.join('app', 'views', controller_file_path)
      end

      def available_views
        %w(index show)
      end
    end
  end
end
