require 'rails/generators/named_base'
require 'rails/generators/resource_helpers'
require 'draper_ext/generators/attributes'

module Rails
  module Generators
    module Jbuilder
      class AttributesGenerator < NamedBase
        include ResourceHelpers
        include DraperExt::Generators::Attributes

        def insert_into_partial_view_files
          # TODO: `destroy`に対応していない。オプション`:before`ならうまくいくかも
          insert_into_file(
            File.join(root_folder, "_#{singular_table_name}.json.jbuilder"),
            ", #{join(*attributes_names)}",
            after: /json.extract! #{singular_name}.object, :id/,
          )
        end

        protected

        def root_folder
          File.join('app', 'views', controller_file_path)
        end
      end
    end
  end
end
