require 'rails/generators/named_base'
require 'rails/generators/resource_helpers'
require 'draper_ext/generators/associations'

module Rails
  module Generators
    module ScaffoldController
      class AssociationsGenerator < NamedBase
        include ResourceHelpers
        include DraperExt::Generators::Associations

        # HACK: レコードパラメータ取得メソッドがコントローラクラスの一番最後に定義されている必要がある
        def insert_into_params_permit
          insert_into_file(
            filepath,
            "\n" + indent(associations_params, 8),
            before: /\s*\)\s*end\s*end\s*\z/,
          )
        end

        protected

        def filepath
          File.join('app', 'controllers', controller_class_path, "#{controller_file_name}_controller.rb")
        end

        def associations_params
          associations.map{|assc|
            "#{assc.name}_attributes: [:id, :_destroy],"
          }.join("\n")
        end
      end
    end
  end
end
