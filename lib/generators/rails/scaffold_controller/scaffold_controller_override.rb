require 'rails/generators/rails/scaffold_controller/scaffold_controller_generator'
require 'draper_ext/generators/hook_for_attributes_and_associations'

Rails::Generators::ScaffoldControllerGenerator.class_eval do
  include DraperExt::Generators::HookForAttributesAndAssociations

  def self.source_paths
    [File.expand_path('../templates', __FILE__)]
  end

  hook_for_arguments :attributes, :associations
end
