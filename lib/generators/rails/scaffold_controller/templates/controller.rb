<%- if namespaced? -%>
require_dependency "<%= namespaced_file_path %>/application_controller"

<%- end -%>
<%- module_namespacing do -%>
class <%= controller_class_name %>Controller < ApplicationController
  before_action :set_ransack, only: [:index, :search]
  before_action :set_<%= plural_name %>, only: [:index]
  before_action :set_persisted_<%= singular_name %>, only: [:show, :edit, :update, :destroy]
  before_action :set_new_<%= singular_name %>, only: [:new, :create]

  decorates_assigned :<%= singular_name %>, :<%= plural_name %>

  # GET <%= route_url %>
  # GET <%= route_url %>.json
  # GET <%= route_url %>.js
  def index
  end

  # GET <%= route_url %>/1
  # GET <%= route_url %>/1.json
  def show
  end

  # GET <%= route_url %>/new
  def new
  end

  # GET <%= route_url %>/1/edit
  def edit
    @<%= orm_instance.name %>.assign_attributes(<%= "#{singular_name}_params" %>)
  end

  # POST <%= route_url %>
  # POST <%= route_url %>.json
  def create
    respond_to do |format|
      if @<%= orm_instance.save %>
        format.html { redirect_to @<%= orm_instance.name %>, notice: <%= "'#{human_name} was successfully created.'" %> }
        format.json { render :show, status: :created, location: @<%= orm_instance.name %> }
      else
        format.html { render :new }
        format.json { render json: <%= "@#{orm_instance.errors}" %>, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT <%= route_url %>/1
  # PATCH/PUT <%= route_url %>/1.json
  def update
    respond_to do |format|
      if @<%= orm_instance.update("#{singular_table_name}_params") %>
        format.html { redirect_to @<%= orm_instance.name %>, notice: <%= "'#{human_name} was successfully updated.'" %> }
        format.json { render :show, status: :ok, location: @<%= orm_instance.name %> }
      else
        format.html { render :edit }
        format.json { render json: <%= "@#{orm_instance.errors}" %>, status: :unprocessable_entity }
      end
    end
  end

  # DELETE <%= route_url %>/1
  # DELETE <%= route_url %>/1.json
  def destroy
    @<%= orm_instance.destroy %>
    respond_to do |format|
      format.html { redirect_to <%= index_helper %>_url, notice: <%= "'#{human_name} was successfully destroyed.'" %> }
      format.json { head :no_content }
    end
  end

  # GET <%= route_url %>/search
  def search
  end

  private

    def set_ransack
      @ransack = <%= class_name %>.ransack(params[:q])
    end

    def set_<%= plural_name %>
      @<%= plural_name %> = @ransack.result.page(params[:page]).per(params[:per])
    end

    def set_persisted_<%= singular_name %>
      @<%= singular_name %> = <%= orm_class.find(class_name, "params[:id]") %>
    end

    def set_new_<%= singular_name %>
      @<%= singular_name %> = <%= orm_class.build(class_name, "#{singular_name}_params") %>
    end

    def <%= singular_name %>_params
      params.fetch(<%= ":#{singular_name}" %>, {}).permit(
      )
    end
end
<%- end -%>
