require 'rails/generators/named_base'
require 'rails/generators/resource_helpers'
require 'draper_ext/generators/attributes'

module Rails
  module Generators
    module ScaffoldController
      class AttributesGenerator < NamedBase
        include ResourceHelpers
        include DraperExt::Generators::Attributes

        def insert_into_params_permit
          insert_into_file(
            filepath,
            indent(join(*attributes_names), 8) + ",\n",
            after: ".permit(\n",
          )
        end

        protected

        def filepath
          File.join('app', 'controllers', controller_class_path, "#{controller_file_name}_controller.rb")
        end
      end
    end
  end
end
