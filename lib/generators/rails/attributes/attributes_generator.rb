require 'rails/generators/base'

module Rails
  module Generators
    class AttributesGenerator < Base
      hook_for :controller, default: "scaffold_controller:attributes"
      hook_for :decorator, default: "decorator:attributes"
      hook_for :template_engine, as: "scaffold:attributes"
      hook_for :jbuilder, default: "jbuilder:attributes"
      hook_for :test_framework
    end
  end
end
