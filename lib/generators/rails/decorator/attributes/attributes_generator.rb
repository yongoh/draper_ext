require 'rails/generators/named_base'
require 'rails/generators/resource_helpers'
require 'draper_ext/generators/attributes'

module Rails
  module Generators
    module Decorator
      class AttributesGenerator < Rails::Generators::NamedBase
        include Rails::Generators::ResourceHelpers
        include DraperExt::Generators::Attributes

        source_root File.expand_path("../templates", __FILE__)

        def insert_class_method_definition_into_model_decorator_file
          insert_into_file(
            File.join(root_folder, "#{singular_name}_model_decorator.rb"),
            indent(attributes_definition_code, 2) + "\n",
            after: /include DraperExt::Model\n/,
          )
        end

        def insert_class_method_definition_into_form_decorator_file
          insert_into_file(
            File.join(root_folder, "#{singular_name}_form_decorator.rb"),
            indent(fields_definition_code, 2) + "\n",
            after: /include DraperExt::Form\n/,
          )
        end

        %w(table article form search_form).each do |context|
          define_method("insert_method_definition_into_#{context}_decorator_file") do
            insert_into_file(
              File.join(root_folder, "#{singular_name}_#{context}_decorator.rb"),
              indent(stemplate("#{context}_decorator.rb.erb"), 2),
              before: /^end/,
            )
          end
        end

        protected

        def root_folder
          File.join('app', 'decorators', controller_file_path)
        end

        def attributes_definition_code
          "attributes #{join(*attributes_with_timestamps.map(&:name))}"
        end

        def fields_definition_code
          "fields #{join(*attributes_names)}"
        end
      end
    end
  end
end
