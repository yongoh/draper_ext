require 'rails/generators/named_base'
require 'rails/generators/resource_helpers'
require 'draper_ext/generators/associations'

module Rails
  module Generators
    module Decorator
      class AssociationsGenerator < Rails::Generators::NamedBase
        include Rails::Generators::ResourceHelpers
        include DraperExt::Generators::Associations

        source_root File.expand_path("../templates", __FILE__)

        %w(model form search_form).each do |context|
          define_method("insert_class_method_definition_into_#{context}_decorator_file") do
            insert_into_file(
              File.join(root_folder, "#{singular_name}_#{context}_decorator.rb"),
              indent(associations_definition_code, 2) + "\n",
              after: /include .+\n/,
            )
          end
        end

        %w(table article form search_form).each do |context|
          define_method("insert_method_definition_into_#{context}_decorator_file") do
            insert_into_file(
              File.join(root_folder, "#{singular_name}_#{context}_decorator.rb"),
              indent(stemplate("#{context}_decorator.rb.erb"), 2),
              before: /^end/,
            )
          end
        end

        protected

        def root_folder
          File.join('app', 'decorators', controller_file_path)
        end

        def associations_definition_code
          "associations #{join(*associations.map(&:name))}"
        end
      end
    end
  end
end
