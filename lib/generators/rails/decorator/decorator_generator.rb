require 'rails/generators/named_base'
require 'rails/generators/resource_helpers'
require 'draper_ext/generators/hook_for_attributes_and_associations'
require 'draper_ext/decoration'

module Rails
  module Generators
    class DecoratorGenerator < NamedBase
      include ResourceHelpers
      include DraperExt::Generators::HookForAttributesAndAssociations

      source_root File.expand_path("../templates", __FILE__)

      class_option :parent, type: :string, desc: "The parent class for the generated decorator"

      def create_root_folder
        empty_directory root_folder
      end

      %w(model table article form search_form).each do |context|
        define_method("create_#{context}_decorator_file") do
          template "#{context}_decorator.rb.erb", File.join(root_folder, "#{singular_name}_#{context}_decorator.rb")
        end
      end

      hook_for_arguments :attributes, :associations
      hook_for :test_framework

      protected

      def root_folder
        File.join('app', 'decorators', controller_file_path)
      end

      def parent_class_name
        options.fetch("parent") do
          DraperExt::Decoration.base_decorator_class
        end
      end
    end
  end
end
