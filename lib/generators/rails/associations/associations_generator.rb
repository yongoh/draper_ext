require 'rails/generators/base'

module Rails
  module Generators
    class AssociationsGenerator < Base
      hook_for :controller, default: "scaffold_controller:associations"
      hook_for :decorator, default: "decorator:associations"
      hook_for :template_engine, as: "scaffold:associations"
      hook_for :jbuilder, default: "jbuilder:associations"
      hook_for :test_framework
    end
  end
end
