require 'generators/reference_field/ajax/ajax_generator'

ReferenceField::Generators::AjaxGenerator.class_eval do
  source_root File.expand_path("../templates", __FILE__)
end
