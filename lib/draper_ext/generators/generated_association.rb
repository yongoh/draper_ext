require 'rails/generators/generated_attribute'

module DraperExt
  module Generators
    class GeneratedAssociation < Rails::Generators::GeneratedAttribute

      SINGULAR_TYPES = %i(references belongs_to has_one).freeze
      COLLECTION_TYPES = %i(has_many has_and_belongs_to_many).freeze

      class << self

        def association_type?(type)
          (SINGULAR_TYPES + COLLECTION_TYPES).include?(type)
        end

        # @param [Array<String>] arguments コマンド引数の配列
        # @return [Array<Array<String>>] 関連名・属性名の順で分割した2つの配列
        def partition_arguments(arguments)
          arguments.partition{|arg| association_type?(arg.slice(/:(.+?)\z/, 1).to_sym) }
        end
      end

      def singular?
        SINGULAR_TYPES.include?(type)
      end

      def collection?
        COLLECTION_TYPES.include?(type)
      end
    end
  end
end
