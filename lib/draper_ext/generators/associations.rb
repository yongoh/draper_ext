require 'draper_ext/generators/argument'
require 'draper_ext/generators/generated_association'

module DraperExt
  module Generators
    module Associations
      include Argument

      def self.included(klass)
        super(klass)
        klass.class_eval do
          argument :associations, type: :array, default: [], banner: 'association:type association:type'
        end
      end

      attr_reader :associations

      def initialize(*args, &block)
        super(*args, &block)
        parse_associations!
      end

      protected

      def parse_associations!
        self.associations = (associations || []).map{|assc| GeneratedAssociation.parse(assc) }
      end
    end
  end
end
