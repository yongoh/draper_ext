require 'draper_ext/generators/argument'
require 'draper_ext/generators/generated_attribute'

module DraperExt
  module Generators
    module Attributes
      include Argument

      def self.included(klass)
        super(klass)
        klass.class_eval do
          argument :attributes, type: :array, default: [], banner: 'field:type field:type'
          class_option :timestamps, type: :boolean, default: true
        end
      end

      def attributes_with_timestamps
        if options[:timestamps]
          attributes + [
            Rails::Generators::GeneratedAttribute.new("created_at", :time, "index"),
            Rails::Generators::GeneratedAttribute.new("updated_at", :time, "index"),
          ]
        else
          attributes
        end
      end
    end
  end
end
