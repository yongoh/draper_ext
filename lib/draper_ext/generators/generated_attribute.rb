Rails::Generators::GeneratedAttribute.include(Module.new do

  def field_matcher(field, value)
    case type
    when :boolean then "with_checkbox(#{field.inspect})"
    when :date then "with_select(#{field.sub(/\]$/, "(1i)]").inspect})"
    when :time then "with_select(#{field.sub(/\]$/, "(4i)]").inspect})"
    else "with_#{field_type}(#{field.inspect}, #{value})"
    end
  end
end)
