require 'draper_ext/generators/generated_association'

module DraperExt
  module Generators
    module HookForAttributesAndAssociations
      extend ActiveSupport::Concern

      included do
        remove_argument :attributes
        argument :attributes_or_associations, type: :array, default: [], banner: 'field:type association:type'
      end

      module ClassMethods

        def hook_for_arguments(*names, **config)
          names.each do |name|
            hook_for name, type: :boolean, default: true, in: [base_name, generator_name].join(":"), **config do |invokator, invokatee|
              arguments = invokator.public_send(name)
              invokator.invoke invokatee, [invokator.name, *arguments] if invokator.behavior == :invoke && arguments.any?
            end
          end
        end
      end

      attr_accessor :attributes, :associations

      protected

      # 引数群を関連名・属性名に分割
      def parse_attributes!
        self.associations, self.attributes = GeneratedAssociation.partition_arguments(attributes_or_associations)
      end
    end
  end
end
