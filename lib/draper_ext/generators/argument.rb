module DraperExt
  module Generators
    module Argument

      private

      def join(*names)
        names.map{|name| ":#{name}" }.join(", ")
      end

      def stemplate(source)
        source = File.expand_path(find_in_source_paths(source))
        ERB.new(File.binread(source), nil, "-").result(binding)
      end

      def indent(str, size)
        str.gsub(/^(.+)$/, "#{' ' * size}\\1")
      end

      %i(insert_into_file gsub_file).each do |method_name|
        define_method(method_name) do |filepath, *args, &block|
          begin
            super(filepath, *args, &block)
          rescue Errno::ENOENT, Thor::Error => err
            puts "The file #{filepath} does not appear to exist"
          end
        end
      end
    end
  end
end
