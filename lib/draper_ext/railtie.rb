module DraperExt
  class Railtie < ::Rails::Railtie

    generators do |app|
      require_relative '../generators/rails/scaffold_controller/scaffold_controller_override'
      require_relative '../generators/erb/scaffold/scaffold_override'
      require_relative '../generators/factory_bot/model/model_override'
      require_relative '../generators/rspec/scaffold/scaffold_override'
      require_relative '../generators/reference_field/ajax/ajax_override'
      require_relative '../generators/rspec/reference_field/ajax/ajax_override'
    end

    initializer "Add features to DraperExt::Model" do
      Model.include(Attribute)
      Model.include(Association)
      Model::ClassMethods.include(Attribute::ClassMethods)
      Model::ClassMethods.include(Association::ClassMethods)
    end

    initializer "Add features to DraperExt::Form" do
      Form.include(Field)
      Form.include(AssociationForm)
      Form::ClassMethods.include(Field::ClassMethods)
      Form::ClassMethods.include(AssociationForm::ClassMethods)
    end

    initializer "Add features to Draper::CollectionDecorator" do
      # PATCH: `#decorates_assigned`に対応
      class << Draper::CollectionDecorator
        alias_method :decorate_collection, :decorate
      end
    end
  end
end
